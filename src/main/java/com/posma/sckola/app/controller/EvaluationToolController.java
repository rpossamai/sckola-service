package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.EvaluationToolBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


/**
 * Created by Francis on 25/04/2017.
 */
@RestController
@RequestMapping("/skola/evaluation/tool")
public class EvaluationToolController {

    static final Logger logger = Logger.getLogger(EvaluationToolController.class);

    static final String MessageEvaluationToolApproveFail = "No fue posible procesar la solicitud";
    static final String MessageEvaluationToolApproveSuccess= "Solicitud procesada";

    @Autowired
    EvaluationToolBF evaluationToolBF;

    @Autowired
    Validation validation;
    
    @Autowired
    SystemMessage systemMessage;

    /**
     * Servicio que permite consultar todas las "herramientas de evaluacion"
     * @return List EvaluationToolDto
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> evaluationToolGetAll() throws IOException {

        MessageDto messageDto = evaluationToolBF.getAll();
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

        /**
         * Funcion de manejo de excepciones pertenecientes a la clase Exception
         * @param exc excepcion interceptada
         * @return una entidad respuesta, con el HttpStatus y el mensaje del error
         */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
