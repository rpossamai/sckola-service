package com.posma.sckola.app.controller;


import com.posma.sckola.app.business.LoginBF;
import com.posma.sckola.app.business.UserBF;
import com.posma.sckola.app.dto.LoginDto;
import com.posma.sckola.app.dto.LoginResultDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.UserDto;
import org.apache.commons.codec.digest.DigestUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import com.posma.sckola.app.security.JwtTokenUtil;

import java.io.IOException;
import java.util.List;


/**
 * Created by Francis on 30/03/2017.
 */
@RestController
@RequestMapping("/skola/login")
public class LoginController {

    static final Logger logger = Logger.getLogger(LoginController.class);

    static final String MessageLoginResourceNotFound = "Recurso solicitado no encontrado";
    static final String MessageLoginApproveFail = "No fue posible procesar la solicitud";
    static final String MessageLoginApproveSuccess= "Solicitud procesada";

    @Autowired
    public UserDetailsService Handler;

    @Autowired
    LoginBF loginBF;

    private AuthenticationManager authenticationManager = new SampleAuthenticationManager();

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    UserBF userBF;
    /**
     * Servicio que permite registrar un nuevo logine en el sistema
     * @return Login ID
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> authenticate(@RequestBody LoginDto loginDto) throws AuthenticationException {
        String passwordEncript = DigestUtils.shaHex(loginDto.getPassword());
        final UserDetails userDetails = Handler.loadUserByUsername(loginDto.getUsername());
        if (userDetails!=null && userDetails.isEnabled())
        {
            if(userDetails.getPassword().equalsIgnoreCase(passwordEncript)){
                    final Authentication authentication = authenticationManager.authenticate(
                            new UsernamePasswordAuthenticationToken(
                                    loginDto.getUsername(),
                                    passwordEncript
                            )
                    );
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    // Reload password post-security so we can generate token
                    final String token = jwtTokenUtil.generateToken(userDetails);


                MessageDto messageDto = userBF.getUser(userDetails.getUsername());
                LoginResultDto loginResultDto = new LoginResultDto();
                loginResultDto.setUserId(((UserDto)messageDto.getResponse()).getId());
                loginResultDto.setUserDetail((UserDto)messageDto.getResponse());
                loginResultDto.setToken(token);

                messageDto.setResponse(loginResultDto);

                return ResponseEntity.ok(messageDto);
            }else{
                MessageDto messageDto = new MessageDto();
                messageDto.setSuccess(false);
                messageDto.setMessage(HttpStatus.UNAUTHORIZED.toString());
                messageDto.setResponse(loginDto);
                return new ResponseEntity<>(messageDto, HttpStatus.UNAUTHORIZED);
            }
        }


        MessageDto messageDto = new MessageDto();
        messageDto.setMessage("Precondition Failed, invalid parameters to login");
        messageDto.setSuccess(false);
        return new ResponseEntity<> (messageDto, HttpStatus.PRECONDITION_FAILED);

/*

        String passwordEncript = DigestUtils.shaHex(request.getPassword());
        final UserDetails userDetails = Handler.loadUserByUsername(request.getUsername());
        if (userDetails!=null && userDetails.isEnabled())
        {
            if (userDetails != null && userDetails.getPassword().equalsIgnoreCase(passwordEncript)) {
                final Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                request.getUsername(),
                                passwordEncript
                        )
                );
                SecurityContextHolder.getContext().setAuthentication(authentication);
                // Reload password post-security so we can generate token
                final String token = jwtTokenUtil.generateToken(userDetails);

                // Return the token
                return ResponseEntity.ok(new LoginResponseDTO(token));
            } else
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        else
            return new ResponseEntity(HttpStatus.PRECONDITION_FAILED);

    } */









}



    /**
     * Funcion de manejo de excepciones pertenecientes a la clase Exception
     * @param exc excepcion interceptada
     * @return una entidad respuesta, con el HttpStatus y el mensaje del error
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }


}
