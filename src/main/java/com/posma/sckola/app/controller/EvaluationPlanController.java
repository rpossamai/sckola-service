package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.EvaluationPlanBF;
import com.posma.sckola.app.dto.EvaluationPlanDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.RequestAssociateDto;
import com.posma.sckola.app.util.SystemMessage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by Francis Ries on 26/04/2017.
 */
@RestController
@RequestMapping("/skola/evaluation/plan")
public class EvaluationPlanController {

    static final Logger logger = Logger.getLogger(EvaluationPlanController.class);

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    EvaluationPlanBF evaluationPlanBF;

    /**
     * Create new evaluation plan
     * @return evaluationPlanDto
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> evaluationGetEvaluationPlan(@RequestBody EvaluationPlanDto evaluationPlanDto) throws IOException{
        MessageDto messageDto;

        if(evaluationPlanDto == null || evaluationPlanDto.getName() == null || evaluationPlanDto.getName().trim() == ""){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = evaluationPlanBF.newEvaluationPlan(evaluationPlanDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Consultar el detalle de un plan de evaluación
     * @return Boolean true
     * @throws IOException
     */
    @RequestMapping(value="/{planId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getEvaluationPlanById(@PathVariable("planId") Long evaluationPlanId) throws IOException{
        MessageDto messageDto;

        if(evaluationPlanId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = evaluationPlanBF.getById(evaluationPlanId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * Consultar lista de planes de evaluacion de un profesor en una comunidad incluyendo los planes que una no hayan sido asignados a materia ninguna materia
     * @return Boolean true
     * @throws IOException
     */
    @RequestMapping(value="/user/{userId}/community/{communityId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getAllEvaluationsPlansFromTeacher(@PathVariable("userId") Long userId,
                                                               @PathVariable("communityId") Long communityId) throws IOException{
        MessageDto messageDto;

        if(communityId == null || userId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = evaluationPlanBF.getAllFromTeacherBycommunity(userId, communityId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Consultar lista de planes de evaluacion de un profesor que aun no hayan sido asignados a materia
     * @return Boolean true
     * @throws IOException
     */
    @RequestMapping(value="/user/{userId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getAllEvaluationsPlansFromTeacher(@PathVariable("userId") Long userId) throws IOException{
        MessageDto messageDto;

        if(userId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = evaluationPlanBF.getAllFromTeacherNoAssociate(userId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * Servicio que permite asociar un plan de evaluación a una materia de la comunidad
     * @return matterCommunitySectionDto
     * @throws IOException
     */
    @RequestMapping(value = "/{planId}/matter_community_section/{matterCommunitySectionId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> associateEvaluationPlan(@PathVariable("planId") Long planId,
                                             @PathVariable("matterCommunitySectionId") Long matterCommunitySectionId,
                                             @RequestBody RequestAssociateDto requestAssociateDto) throws IOException {

        if(planId == null || matterCommunitySectionId == null || requestAssociateDto == null || requestAssociateDto.getAssociate() == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = evaluationPlanBF.associateEvaluationPlanClass(planId, matterCommunitySectionId, requestAssociateDto.getAssociate().booleanValue());
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }



    /**
     * Create new evaluation plan from a evaluation plan template
     * @return evaluationPlanDto
     * @throws IOException
     */
    @RequestMapping(value = "/template/{planTemplateIdToCopy}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> createFromTemplate(@PathVariable("planTemplateIdToCopy") Long planTemplateIdToCopy) throws IOException {

        if(planTemplateIdToCopy == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = evaluationPlanBF.newEvaluationPlanFromTemplate(planTemplateIdToCopy);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value="/{planTemplateId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> updteFromTemplate(@PathVariable("planTemplateId") Long planTemplateId,
                                               @RequestBody EvaluationPlanDto evaluationPlanDto) throws IOException{

        if(planTemplateId == null || evaluationPlanDto == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = evaluationPlanBF.updateEvaluationPlan(planTemplateId,evaluationPlanDto);

        if(messageDto.getSuccess())
        return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

}
