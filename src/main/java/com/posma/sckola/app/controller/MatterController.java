package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.MatterBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.RequestAssociateDto;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


/**
 * Created by Francis on 20/04/2017.
 */
@RestController
@RequestMapping("/skola/matter")
public class MatterController {

    static final Logger logger = Logger.getLogger(MatterController.class);

    static final String MessageMatterApproveFail = "No fue posible procesar la solicitud";
    static final String MessageMatterApproveSuccess= "Solicitud procesada";

    @Autowired
    MatterBF matterBF;

    @Autowired
    Validation validation;
    
    @Autowired
    SystemMessage systemMessage;


    /**
     * Servicio que permite consultar todas las "materias plantillas"
     * @return List MatterDto
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterGetAll() throws IOException {

        MessageDto messageDto = matterBF.matterGetAll();
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    /**
     * Servicio consultar el detalle de una "materias plantillas"
     * @return List MatterDto
     * @throws IOException
     */
    @RequestMapping(value="/{matterId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterGetById(@PathVariable("matterId") Long matterId) throws IOException {

        MessageDto messageDto;

        if(matterId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterBF.matterGetById(matterId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }


    /**
     * Servicio que permite consultar todas las "materias plantillas"
     * @return List MatterDto
     * @throws IOException
     */
    /*
    @RequestMapping(value="/{matterId}/community/{communityId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterGetById(@PathVariable("matterId") Long matterId,
                                           @PathVariable("communityId") Long communityId) throws IOException {

        MessageDto messageDto;

        if(matterId == null || communityId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterBF.matterGetByIdCommunityId(matterId, communityId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    /*

    /**
     * Servicio que permite consultar todas las materias de una comunidad
     * @return List MatterDto
     * @throws IOException
     */

    @RequestMapping(value="/community/{communityId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterGetAllByCommunity(@PathVariable("communityId") Long communityId) throws IOException {

        MessageDto messageDto;

        if(communityId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterBF.matterGetAllByCommunity(communityId);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * listar materia existentes en la comunidad, donde un profesor(dicta) o como alumno(asiste)
     * sirve para asignar una evaluacion, consultar todas las clases que da un profesor
     * @return List MatterDto
     * @throws IOException
     */

    @RequestMapping(value="/community/{communityId}/role/{roleId}/user/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterGetAllByCommunityUser(@PathVariable("communityId") Long communityId,
                                                     @PathVariable("roleId") Long roleId,
                                                     @PathVariable("userId") Long userId) throws IOException {

        MessageDto messageDto;

        if(communityId == null || roleId == null || userId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterBF.matterCommunityGetAllByUserRole(communityId, roleId, userId);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * listar materia existentes en la comunidad, donde un profesor(dicta) o como alumno(asiste)
     * sirve para asignar una evaluacion, consultar todas las clases que da un profesor
     * @return List MatterDto
     * @throws IOException
     */

    @RequestMapping(value="/community/{communityId}/role/{roleId}/user/{userId}/type/{type}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterGetAllByCommunityUser(@PathVariable("communityId") Long communityId,
                                                         @PathVariable("roleId") Long roleId,
                                                         @PathVariable("userId") Long userId,
                                                         @PathVariable("type") String type) throws IOException {

        MessageDto messageDto;

        if(communityId == null || roleId == null || userId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterBF.matterCommunityGetAllByUserRoleByType(communityId, roleId, userId, type);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * listar materia existentes en la comunidad, donde un profesor(dicta) o como alumno(asiste)
     * sirve para asignar una evaluacion, consultar todas las clases que da un profesor sin evaluacion
     * @return List MatterDto
     * @throws IOException
     */

    @RequestMapping(value="/community/{communityId}/role/{roleId}/user/{userId}/without_evaluation_plan", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterGetAllByCommunityUserWithoutEvaluationPlan(@PathVariable("communityId") Long communityId,
                                                         @PathVariable("roleId") Long roleId,
                                                         @PathVariable("userId") Long userId) throws IOException {

        MessageDto messageDto;

        if(communityId == null || roleId == null || userId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterBF.matterCommunityGetAllByUserRoleWithoutEvaluationPlan(communityId, roleId, userId);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * Servicio que permite asociar una materia a una comunidad
     * @return List MatterDto
     * @throws IOException
     */
    @RequestMapping(value = "/{matterId}/community/{communityId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> matterAssociate(@PathVariable("matterId") Long matterId,
                                             @PathVariable("communityId") Long communityId,
                                             @RequestBody RequestAssociateDto requestAssociateDto) throws IOException {

        if(matterId == null || communityId == null || requestAssociateDto == null || requestAssociateDto.getAssociate() == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = matterBF.matterAssociate(matterId, communityId, requestAssociateDto.getAssociate().booleanValue() );
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    /**
     * Listar todas las materias impartidas por un profesor, de todas las comunidades
     * sirve para listar las materias y seccion que da un profesor. cuando aun no ha seleccionado una communidad
     * @return List MatterDto
     * @throws IOException
     */
    @RequestMapping(value="/user/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterGetAllByTeacher(@PathVariable("userId") Long userId) throws IOException {

        MessageDto messageDto;

        if(userId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterBF.matterCommunityGetAllByTeacher(1L, userId);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * Servicio que permite crear un Usuario en skola
     * @return List MatterDto
     * @throws IOException
     */
    /*
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> matterCreate(@RequestBody MatterDto matterDto) throws IOException {

        if(matterDto == null || matterDto.getMail() == null || !validation.isEmail(matterDto.getMail()) || matterDto.getPassword() == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = matterBF.matterCreate(matterDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }
*/



    
        /**
         * Funcion de manejo de excepciones pertenecientes a la clase Exception
         * @param exc excepcion interceptada
         * @return una entidad respuesta, con el HttpStatus y el mensaje del error
         */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
