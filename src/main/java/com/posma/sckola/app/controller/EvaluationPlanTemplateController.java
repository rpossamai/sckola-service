package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.EvaluationPlanTemplateBF;
import com.posma.sckola.app.dto.EvaluationPlanDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.util.SystemMessage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by Francis Ries on 27/04/2017.
 */
@RestController
@RequestMapping("/skola/template/evaluation/plan")
public class EvaluationPlanTemplateController {

    static final Logger logger = Logger.getLogger(EvaluationPlanTemplateController.class);

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    EvaluationPlanTemplateBF evaluationPlanTemplateBF;

    /**
     * Create new evaluation plan template
     * @return EvaluationPlanDto
     * @throws IOException
     */
    @RequestMapping(value="/{planIdToCopy}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> evaluationGetEvaluationPlan(@PathVariable("planIdToCopy") Long evaluationPlanId,
                                                         @RequestBody EvaluationPlanDto evaluationPlanDto) throws IOException{
        MessageDto messageDto;

        if(evaluationPlanId == null || evaluationPlanDto == null
                || evaluationPlanDto.getName() == null
                || evaluationPlanDto.getName().trim() == ""){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = evaluationPlanTemplateBF.newEvaluationPlanTemplate(evaluationPlanId, evaluationPlanDto.getName());
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Busca todas las plantillas de plan de evaluación
     * @return List EvaluationPlanDto
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getAllEvaluationPlanTemplate() throws IOException{
        MessageDto messageDto;

        messageDto = evaluationPlanTemplateBF.getAll();
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }
}
