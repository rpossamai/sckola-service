package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.QualificationBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.QualificationUserDto;
import com.posma.sckola.app.util.SystemMessage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
@RestController
@RequestMapping("/skola/qualification")
public class QualificationController {

    static final Logger logger = Logger.getLogger(QualificationController.class);

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    QualificationBF qualificationBF;

    /**
     * Servicio que permite crear las qualification de todos los estudiantes de una matter_community_section a una evaluación
     * @return Boolean true
     * @throws IOException
     */
    @RequestMapping(value="/matter_community_section/{matterId}/evaluation/{evaluationId}",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> qualificationCreate(@PathVariable("matterId") Long matterComunitySectionId,
                                                 @PathVariable("evaluationId") Long evaluationId) throws IOException{

        MessageDto messageDto;

        if(matterComunitySectionId == null || evaluationId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = qualificationBF.createQualification(matterComunitySectionId,evaluationId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);

    }

    @RequestMapping(value="/{id}",method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> qualificationUpdate(@PathVariable("id") Long qualificationId,
                                                 @RequestBody QualificationUserDto qualificationUserDto) throws IOException{

        MessageDto messageDto;

        if(qualificationUserDto == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = qualificationBF.updateQualification(qualificationId,qualificationUserDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);

    }

    /**
     * Servicio que permite buscar todas las calificaciones del estudiante
     * @return Boolean true
     * @throws IOException
     */
    @RequestMapping(value="/matter_community_section/{matterId}/user/{userId} ",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> qualificationGetStudent(@PathVariable("matterId") Long matterComunitySectionId,
                                                  @PathVariable("userId") Long studentId) throws IOException{
        MessageDto messageDto;

        if(matterComunitySectionId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = qualificationBF.qualificationGetStudent(matterComunitySectionId, studentId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

}
