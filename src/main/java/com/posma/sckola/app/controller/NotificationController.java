package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.util.SystemMessage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Posma-dev on 23/08/2017.
 */
@RestController
@RequestMapping("skola/notification")
public class NotificationController {

    static final Logger logger = Logger.getLogger(MatterController.class);

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    NotificationBF notificationBF;

    /**
     * Gets the notifications of an user in a community.
     * @param userMail Email of the user.
     * @param id Id of the community to search notifications in.
     * @return ResponseEntity of DashboardNotificationDto
     */
    @RequestMapping(method = RequestMethod.GET, value={"/{id}"})
    @ResponseBody
    public ResponseEntity<?> getNotifications(@RequestParam("email") String userMail,@PathVariable("id")Long id){
        MessageDto response;
        if(userMail == null){
            response = new MessageDto();
            response.setSuccess(false);
            response.setErrorCode("SK-002");
            response.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        response = notificationBF.getActiveNotificationsByUser(userMail,id);
        if(response.getSuccess()){
            return ResponseEntity.ok(response);
        }
        return new ResponseEntity<>(response,HttpStatus.CONFLICT);
    }


    @RequestMapping(method= RequestMethod.GET, value = {"/history/{id}"})
    public ResponseEntity<?> getHistoryNotification(@RequestParam("email") String userMail, @PathVariable("id") Long communityId){
        MessageDto response;
        if(userMail == null){
            response = new MessageDto();
            response.setSuccess(false);
            response.setErrorCode("SK-002");
            response.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        response = notificationBF.getHistoryNotificationByUser(userMail,communityId);
        if(response.getSuccess()){
            return ResponseEntity.ok(response);
        }
        return new ResponseEntity<>(response,HttpStatus.CONFLICT);
    }

    /**
     * Sets a notification to inactive.
     * @param id Id of the notification.
     * @return ResponseEntity
     */
    @RequestMapping(value="/remove/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> inactivateNotification(@PathVariable("id")Long id){
        MessageDto response;
        if(id == null){
            response = new MessageDto();
            response.setSuccess(false);
            response.setErrorCode("SK-002");
            response.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        response = notificationBF.removeActiveNotification(id);
        if(response.getSuccess()){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(response,HttpStatus.CONFLICT);
    }
}
