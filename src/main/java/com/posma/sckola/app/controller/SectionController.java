package com.posma.sckola.app.controller;


import com.posma.sckola.app.business.SectionBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.SectionDto;
import com.posma.sckola.app.persistence.entity.SectionType;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


/**
 * Created by Francis on 24/04/2017.
 */
@RestController
@RequestMapping("/skola/section")
public class SectionController {

    static final Logger logger = Logger.getLogger(SectionController.class);

    static final String MessagesectionApproveFail = "No fue posible procesar la solicitud";
    static final String MessagesectionApproveSuccess= "Solicitud procesada";

    @Autowired
    SectionBF sectionBF;

    @Autowired
    Validation validation;

    @Autowired
    SystemMessage systemMessage;

    /**
     * Service query all sections from a community with status "ACTIVE"
     * @return List sectionDto
     * @throws java.io.IOException
     */

    @RequestMapping(value="/community/{communityId}/userId/{userId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> sectionCommunityCreate(@PathVariable("communityId") Long communityId,
                                                    @PathVariable("userId") Long userId,
                                                    @RequestBody SectionDto sectionDto) throws IOException {

        MessageDto messageDto = new MessageDto();;

        // validate input parameters
        try{
            if(communityId == null || sectionDto == null
                    || sectionDto.getName() == null
                    || sectionDto.getName().trim() == ""){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
            }
        }catch (IllegalArgumentException i){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }


        messageDto = sectionBF.sectionCommunityCreate(communityId, sectionDto, userId);


        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * Service query all sections from a community with status "ACTIVE"
     * @return List sectionDto
     * @throws java.io.IOException
     */

    @RequestMapping(value="/community/{communityId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> sectionCommunityGetAll(@PathVariable("communityId") Long communityId) throws IOException {

        MessageDto messageDto;

        if(communityId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = sectionBF.sectionCommunityGetAll(communityId);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

        /**
         * Funcion de manejo de excepciones pertenecientes a la clase Exception
         * @param exc excepcion interceptada
         * @return una entidad respuesta, con el HttpStatus y el mensaje del error
         */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
