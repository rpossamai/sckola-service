package com.posma.sckola.app.dto;

import java.util.List;

/**
 * Created by Posma-dev on 04/09/2017.
 */
public class NotificationHistoryDto {
    private List<DashboardNotificationDto> notifications;
    private int activeNotifications;

    public NotificationHistoryDto(List<DashboardNotificationDto> notifications, int activeNotifications) {
        this.notifications = notifications;
        this.activeNotifications = activeNotifications;
    }

    public NotificationHistoryDto() {
    }

    public List<DashboardNotificationDto> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<DashboardNotificationDto> notifications) {
        this.notifications = notifications;
    }

    public int getActiveNotifications() {
        return activeNotifications;
    }

    public void setActiveNotifications(int activeNotifications) {
        this.activeNotifications = activeNotifications;
    }
}
