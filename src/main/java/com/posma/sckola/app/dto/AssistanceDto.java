package com.posma.sckola.app.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Posma-ricardo on 21/04/2017.
 */
public class AssistanceDto implements Serializable {

    private String date;

    private Long matterCommunitySectionId;

    private List<UserAssistDto> userAssistList;

    public AssistanceDto(){}

    public String getDate(){return date;}

    public void setDate(String date){this.date = date;}

    public Long getMatterCommunitySectionId(){return matterCommunitySectionId;}

    public void setMatterCommunitySectionId(Long matterCommunitySectionId){
        this.matterCommunitySectionId = matterCommunitySectionId;}

    public List<UserAssistDto> getUserAssistList(){return userAssistList;}

    public void setUserAssistList(List<UserAssistDto> userAssistList){
        this.userAssistList = userAssistList;
    }
}
