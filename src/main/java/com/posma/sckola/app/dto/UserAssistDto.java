package com.posma.sckola.app.dto;

/**
 * Created by Posma-ricardo on 21/04/2017.
 */
public class UserAssistDto {

    private Long userId;

    private String firstName;

    private String lastName;

    private Boolean assistance;

    public UserAssistDto(){}

    public Long getUserId(){return userId;}

    public void setUserId(Long userId){this.userId = userId;}

    public String getFirstName(){return firstName;}

    public void setFirstName(String firstName){this.firstName = firstName;}

    public String getLastName(){return lastName;}

    public void setLastName(String lastName){this.lastName = lastName;}

    public Boolean getAssistance(){return assistance;}

    public void setAssistance(Boolean assistance){this.assistance = assistance;}

    @Override
    public String toString(){
        return "{ \n" +
                " 'userId':" + userId + ",\n" +
                " 'firstName':" + firstName + ",\n" +
                " 'lastName':" + lastName + ",\n" +
                " 'assistance':'" + assistance + "',\n" +
                "}";
    }
}
