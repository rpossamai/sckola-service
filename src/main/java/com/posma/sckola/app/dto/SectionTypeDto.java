package com.posma.sckola.app.dto;

/**
 * Created by Posma-ricardo on 04/05/2017.
 */
public class SectionTypeDto {

    private String type;

    public SectionTypeDto(){}

    public String getType(){return type;}

    public void setType(String type){this.type = type;}
}
