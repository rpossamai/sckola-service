package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Posma-ricardo on 10/04/2017.
 */
public class CommunityDto implements Serializable {

    private Long id;

    private String name;

    private String description;

    private CommunityDto communityOrigin;

    public CommunityDto(){}

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public String getName(){return name;}

    public void setName(String name){this.name = name;}

    public String getDescription(){return description;}

    public void setDescription(String description){this.description = description;}

    public CommunityDto getCommunityOrigin(){return communityOrigin;}

    public void setCommunityOrigin(CommunityDto communityOrigin){this.communityOrigin = communityOrigin;}

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'name':'" + name + "',\n" +
                " 'description': '" + description + "',\n" +
                " 'communityOrigin':" + communityOrigin + "',\n" +
                "}";
    }
}
