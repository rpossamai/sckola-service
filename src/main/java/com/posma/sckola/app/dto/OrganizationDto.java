package com.posma.sckola.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 27/03/2017.
 */

public class OrganizationDto implements Serializable {
    private Long id;

    private String taxIdentity;

    private String businessName;

    private String email;

    private String address;

    private List<PhoneDto> phoneList = new ArrayList<PhoneDto>();


    public OrganizationDto() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaxIdentity() {
        return taxIdentity;
    }

    public void setTaxIdentity(String taxIdentity) {
        this.taxIdentity = taxIdentity;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PhoneDto> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<PhoneDto> phoneList) {
        this.phoneList = phoneList;
    }

    public boolean addPhoneList(PhoneDto phoneDto) {
        return phoneList.add(phoneDto);
    }

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'taxIdentity':'" + taxIdentity + "',\n" +
                " 'businessName':'" + businessName + "',\n" +
                " 'email':'" + email + "',\n" +
                " 'address':'" + address + "',\n" +
                " 'users': " + phoneList.toString() + " \n" +
                "}";
    }

}
