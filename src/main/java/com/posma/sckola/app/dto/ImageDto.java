package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Ricardo on 19/05/2017.
 */
public class ImageDto implements Serializable {

    private String direccion;

    private String nombre;

    public ImageDto(){}

    public String getDireccion(){ return direccion;}

    public void setDireccion(String direccion){this.direccion = direccion;}

    public String getNombre(){return nombre;}

    public void setNombre(String nombre){this.nombre = nombre;}
}
