package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 20/04/2017.
 */

public class MatterDto implements Serializable {

    private Long id;

    private String name;

    private String description;

    private String objective;

    private String especificObjective;


    public MatterDto() {

    }

    public MatterDto(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getEspecificObjective() {
        return especificObjective;
    }

    public void setEspecificObjective(String especificObjective) {
        this.especificObjective = especificObjective;
    }

}
