package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 20/04/2017.
 */

public class MatterCommunitySectionDto implements Serializable {


    private Long id;

    private Long teacherId;

    private MatterCommunityDto matterCommunity;

    private SectionDto section;

    private String type;

    private EvaluationPlanDto evaluationPlan;

    public MatterCommunitySectionDto() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTeacherId(){return teacherId;}

    public void setTeacherId(Long teacherId){this.teacherId = teacherId;}

    public MatterCommunityDto getMatterCommunity() {
        return matterCommunity;
    }

    public void setMatterCommunity(MatterCommunityDto matterCommunity) {
        this.matterCommunity = matterCommunity;
    }

    public SectionDto getSection() {
        return section;
    }

    public void setSection(SectionDto section) {
        this.section = section;
    }

    public String getType(){return type;}

    public void setType(String type){this.type = type;}

    public EvaluationPlanDto getEvaluationPlan() {
        return evaluationPlan;
    }

    public void setEvaluationPlan(EvaluationPlanDto evaluationPlan) {
        this.evaluationPlan = evaluationPlan;
    }

}
