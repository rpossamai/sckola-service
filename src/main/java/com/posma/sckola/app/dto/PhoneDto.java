package com.posma.sckola.app.dto;


import java.io.Serializable;

/**
 * Created by Francis Ries on 27/03/2017.
 */

public class PhoneDto implements Serializable {

    private Long id;

    private String phone;

    public PhoneDto(){}

    public PhoneDto(String phone){this.phone = phone;}

    public Long getId(){return id;}

    public void setId(Long id) {this.id = id;}

    public String getPhone(){return phone;}

    public void setPhone(String phone){ this.phone = phone;}

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'phone':'" + phone + "',\n" +
                "}";
    }
}
