package com.posma.sckola.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 26/04/2017.
 */
public class EvaluationPlanDto implements Serializable {

    private Long id;

    private String name;

    private Long userId;

    private List<EvaluationDto> evaluations = new ArrayList<EvaluationDto>();

    private MatterCommunitySectionDto matterCommunitySection;

    public EvaluationPlanDto(){}

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public Long getUserId(){return userId;}

    public void setUserId(Long userId){this.userId = userId;}

    public String getName(){return name;}

    public void setName(String name){this.name = name;}

    public List<EvaluationDto> getEvaluations(){return evaluations;}

    public void setEvaluations(List<EvaluationDto> evaluations){this.evaluations = evaluations;}

    public boolean addEvaluations(EvaluationDto evaluation){ return evaluations.add(evaluation);}

    public MatterCommunitySectionDto getMatterCommunitySection(){return matterCommunitySection;}

    public void setMatterCommunitySection(MatterCommunitySectionDto matterCommunitySection){this.matterCommunitySection = matterCommunitySection;}

}
