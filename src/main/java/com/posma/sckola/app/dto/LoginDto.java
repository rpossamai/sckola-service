package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 27/03/2017.
 */
public class LoginDto implements Serializable {


    private String username;

    private String password;


    public LoginDto() { }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString(){
        return "{ \n" +
                " 'username': '" + username + "',\n" +
                " 'password':' ******* '\n" +
                "}";
    }
}
