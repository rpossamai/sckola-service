package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 24/04/2017.
 */
public class RoleDto implements Serializable {

    private Long id;

    private String name;

    private String description;


    public RoleDto() {

    }

    public RoleDto(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
