package com.posma.sckola.app.dto;

import java.io.Serializable;

/**
 * Created by Francis Ries on 29/05/2017.
 */
public class WizardDto implements Serializable {

    private Long userId;

    private Long profileUser;

    private String stepName;

    private CommunityDto community;

    private MatterCommunitySectionDto matterCommunitySection;

    private EvaluationPlanDto evaluationPlan;

    public WizardDto() { }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public CommunityDto getCommunity() {
        return community;
    }

    public void setCommunity(CommunityDto community) {
        this.community = community;
    }

    public MatterCommunitySectionDto getMatterCommunitySection() {
        return matterCommunitySection;
    }

    public void setMatterCommunitySection(MatterCommunitySectionDto matterCommunitySection) {
        this.matterCommunitySection = matterCommunitySection;
    }

    public Long getProfileUser() {
        return profileUser;
    }

    public void setProfileUser(long profileUser) {
        this.profileUser = profileUser;
    }
    public EvaluationPlanDto getEvaluationPlan() {
        return evaluationPlan;
    }

    public void setEvaluationPlan(EvaluationPlanDto evaluationPlan) {
        this.evaluationPlan = evaluationPlan;
    }

    @Override
    public String toString(){
        return "{ \n" +
                " 'userId': " + userId + ",\n" +
                " 'stepName':'" + stepName +" \n" +
                " 'community': " + community != null?community.toString():"null" +" \n" +
                " 'matterCommunitySection': " + matterCommunitySection != null?matterCommunitySection.toString():"null" +" \n" +
                " 'evaluationPlan': "+ evaluationPlan.toString() +" \n" +
                "}";
    }
}
