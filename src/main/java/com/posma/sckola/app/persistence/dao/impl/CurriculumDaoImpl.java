package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.CurriculumDao;
import com.posma.sckola.app.persistence.entity.CurriculumEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("CurriculumDao")
public class CurriculumDaoImpl extends AbstractDao<CurriculumEntity> implements CurriculumDao {

    public CurriculumDaoImpl(){
        super ();
        setClazz(CurriculumEntity.class);
    }


}
