package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.SystemMessageDao;
import com.posma.sckola.app.persistence.entity.SystemMessageEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Repository("SystemMessageDao")
public class SystemMessageDaoImpl extends AbstractDao<SystemMessageEntity> implements SystemMessageDao {

    public SystemMessageDaoImpl(){
        super();
        setClazz(SystemMessageEntity.class);
    }

}
