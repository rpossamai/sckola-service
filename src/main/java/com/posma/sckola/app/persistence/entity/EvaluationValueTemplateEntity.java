package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 127/04/2017.
 */
@Entity
@Table(name = "EVALUATION_VALUE_TEMPLATE")
public class EvaluationValueTemplateEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "EVALUATION_VALUE_TEMPLATE_SEQ", sequenceName = "S_ID_EVALUATION_VALUE_TEMPLATE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVALUATION_VALUE_TEMPLATE_SEQ")
    private Long id;

    @Embedded
    private Qualification qualification = new Qualification();

    @ManyToOne
    @JoinColumn(name="EVALUATION_SCALE_TEMPLATE_ID", nullable = false)
    private EvaluationScaleTemplateEntity evaluationScaleTemplate;

    public EvaluationValueTemplateEntity() {

    }

    public EvaluationValueTemplateEntity(Qualification qualification, EvaluationScaleTemplateEntity evaluationScaleTemplate) {
        this.qualification = qualification;
        this.evaluationScaleTemplate = evaluationScaleTemplate;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    public EvaluationScaleTemplateEntity getEvaluationScaleTemplate() {
        return evaluationScaleTemplate;
    }

    public void setEvaluationScaleTemplate(EvaluationScaleTemplateEntity evaluationScaleTemplate) {
        this.evaluationScaleTemplate = evaluationScaleTemplate;
    }




}
