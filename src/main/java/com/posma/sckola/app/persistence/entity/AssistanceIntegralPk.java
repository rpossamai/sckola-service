package com.posma.sckola.app.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Posma-ricardo on 21/04/2017.
 */
@Embeddable
public class AssistanceIntegralPk implements Serializable {
    @Column(name = "USER_ID", nullable = false)
    private Long user;

    @Column(name = "SECTION_ID", nullable = false)
    private Long sectionId;

    @Column(name = "DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;

    public AssistanceIntegralPk() {}

    public AssistanceIntegralPk(Long user, Long sectionId, Date date) {
        this.user = user;
        this.sectionId = sectionId;
        this.date = date;
    }

    public Long getUser(){
        return user;
    }

    public void setUser(Long user){
        this.user = user;
    }

    public Long getSectionId(){
        return sectionId;
    }

    public void setSectionId(Long sectionId){
        this.sectionId = sectionId;
    }

    public Date getDate(){
        return date;
    }

    public void setDate(Date date){
        this.date = date;
    }

    @Override
    public boolean equals(Object assistancePk){
        try {
            if (this.date.equals(((AssistanceIntegralPk)assistancePk).getDate()) &&
                    this.user.equals(((AssistanceIntegralPk)assistancePk).getUser()) &&
                    this.sectionId.equals(((AssistanceIntegralPk)assistancePk).getSectionId()))
                return true;
        }catch(NullPointerException npe){

        }
        return false;
    }

    // equals, hashCode
}
