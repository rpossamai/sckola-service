package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.NotificationDao;
import com.posma.sckola.app.persistence.entity.MatterCommunitySectionEntity;
import com.posma.sckola.app.persistence.entity.NotificationEntity;

import com.posma.sckola.app.persistence.entity.NotificationStatus;
import com.posma.sckola.app.persistence.entity.NotificationType;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by Posma-dev on 22/08/2017.
 */
@Repository("NotificationDao")
public class NotificationDaoImpl extends AbstractDao<NotificationEntity> implements NotificationDao {

    public NotificationDaoImpl() {
        super();
        setClazz(NotificationEntity.class);
    }

    public NotificationEntity findByDateAndSectionAssistanceNotification(Date date, MatterCommunitySectionEntity mattersection){
        Query query = this.getEntityManager().createQuery("SELECT m FROM NotificationEntity as m where m.date = :date and m.matter = :matterSection and m.type = :typeNotification");
        query.setParameter("date",date);
        query.setParameter("matterSection", mattersection);
        query.setParameter("typeNotification", NotificationType.ASSISTANCE);
        NotificationEntity result = (NotificationEntity) query.getSingleResult();
        return result;
    }

    /**
     * Find all notifications with status NOT_SENDED for an user.
     *
     * @param mail
     * @return List of NotificationEntity
     */
    public List<NotificationEntity> findByUser(String mail) {
        NotificationStatus status = NotificationStatus.NOT_SENDED;
        Query query = this.getEntityManager().createQuery("SELECT m FROM NotificationEntity as m where m.user.mail ='" + mail + "' and m.status = :status");
        query.setParameter("status", status);
        return query.getResultList();
    }

    /**
     * Find all active notification in a community for a user.
     * @param username Email of the user
     * @param communityId Id of the community
     * @return List of NotificationEntity
     */

    public List<NotificationEntity> findActiveNotificationByUserByCommunity(String username, Long communityId) {
        NotificationStatus status = NotificationStatus.SENDED;
        Query query = this.getEntityManager().createQuery("SELECT m FROM NotificationEntity as m where m.user.mail ='" + username + "' and m.status = :status and m.active = :active and m.community.id = :communityId");
        //query.setParameter("mail",userName);
        query.setParameter("status", status);
        query.setParameter("active", true);
        query.setParameter("communityId", communityId);

        return query.getResultList();
    }

    public List<NotificationEntity> findHistoryNotification(String username, Long communityId){
        Query query = this.getEntityManager().createQuery("SELECT m FROM NotificationEntity as m where m.user.mail ='" + username + "' and m.community.id = :communityId");
        //query.setParameter("mail",userName);
        query.setParameter("communityId", communityId);
        return query.getResultList();
    }

    /**
     * Find notification by a notification used to check if a notification already exists
     *
     * @param notification notification to find.
     * @return
     */
    public List<NotificationEntity> findByNotification(NotificationEntity notification) {
        Query query = this.getEntityManager().createQuery("SELECT m FROM NotificationEntity as m where m.user = :user " +
                "and m.matter = :matter and m.community = :community  and m.typeName=:typeName and m.type=:type and " +
                "m.date = :date");
        query.setParameter("user", notification.getUser());
        query.setParameter("matter", notification.getMatter());
        query.setParameter("community", notification.getCommunity());
        query.setParameter("typeName", notification.getTypeName());
        query.setParameter("type", notification.getType());
        query.setParameter("date", notification.getDate());
        return query.getResultList();
    }

}
