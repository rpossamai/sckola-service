package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.CommunityDao;
import com.posma.sckola.app.persistence.entity.CommunityEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Posma-ricardo on 10/04/2017.
 */
@Repository("CommunityDao")
public class CommunityDaoImpl extends AbstractDao<CommunityEntity> implements CommunityDao {

    public CommunityDaoImpl(){
        super();
        setClazz(CommunityEntity.class);
    }
}
