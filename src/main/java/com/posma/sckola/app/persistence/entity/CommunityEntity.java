package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Posma-ricardo on 10/04/2017.
 */
@Entity
@Table(name = "COMMUNITY",
        indexes = {
                @Index(columnList ="NAME",name = "IDX_NAME_COMMUNITY")
        })
public class    CommunityEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name="COMMUNITY_SEQ", sequenceName="S_ID_COMMUNITY", initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMUNITY_SEQ")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(mappedBy="community", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserRoleCommunityEntity> userRoleCommunityList;

/*
    @OneToMany(mappedBy="community", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<StudentEntity> studentList;
*/

    @OneToMany(mappedBy="community", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<MatterCommunityEntity> matterCommunityList;


    public CommunityEntity(){}

    public CommunityEntity(String name, String description){
        this.name = name;
        this.description = description;
    }

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public String getName(){return name;}

    public void setName(String name){this.name = name;}

    public String getDescription(){return description;}

    public void setDescription(String description){this.description = description;}

    public List<UserRoleCommunityEntity> getUserRoleCommunityList() {
        return userRoleCommunityList;
    }
    public void setUserRoleCommunityList(List<UserRoleCommunityEntity> userRoleCommunityList) {
        this.userRoleCommunityList = userRoleCommunityList;
    }
    public boolean addUserRoleCommunityList(UserRoleCommunityEntity userRoleCommunity) {
        return userRoleCommunityList.add(userRoleCommunity);
    }


    /*
        public List<StudentEntity> getStudentList() {
            return studentList;
        }

        public void setStudentList(List<StudentEntity> studentList) {
            this.studentList = studentList;
        }

        public boolean addStudentList(StudentEntity student) {
            return studentList.add(student);
        }
    */
    public List<MatterCommunityEntity> getMatterCommunityList() {
        return matterCommunityList;
    }

    public void setMatterCommunityList(List<MatterCommunityEntity> matterCommunityList) {
        this.matterCommunityList = matterCommunityList;
    }

    public boolean addMatterCommunityList(MatterCommunityEntity matterCommunity) {
        return matterCommunityList.add(matterCommunity);
    }

}
