package com.posma.sckola.app.persistence.entity;


/**
 * Created by Francis on 27/03/2017.
 *
 * UNASSIGNED: usuario creado que para pertenecer a una comunidad, pero no posee cuenta de acceso, este estatus lo tiene un alumno creado por un profesor
 * TO_VALIDATE: cuenta creada, esperando confirmacion de correo para activarse
 * WIZARD: cuenta activa en proceso de cumplir con los primeros pasos
 * ACTIVE: usuario activo en el sistema
 * INACTIVE: cuenta
 * LOCKED: cuenta bloqueada por inactividad, o no solicitud de otro usuario
 */
public enum StatusUser {
    UNASSIGNED,TO_VALIDATE,WIZARD,ACTIVE,INACTIVE,LOCKED

}
