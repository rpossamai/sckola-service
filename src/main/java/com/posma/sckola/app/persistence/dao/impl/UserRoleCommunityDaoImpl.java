package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.UserRoleCommunityDao;
import com.posma.sckola.app.persistence.entity.SectionEntity;
import com.posma.sckola.app.persistence.entity.Status;
import com.posma.sckola.app.persistence.entity.UserEntity;
import com.posma.sckola.app.persistence.entity.UserRoleCommunityEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Repository("UserRoleCommunityDao")
public class UserRoleCommunityDaoImpl extends AbstractDao<UserRoleCommunityEntity> implements UserRoleCommunityDao {


    @PersistenceContext
    private EntityManager entityManager;

    public UserRoleCommunityDaoImpl() {
        super();
        setClazz(UserRoleCommunityEntity.class);
    }

    /**
     * description
     *
     * @param userId
     * @param roleId
     * @return List<UserRoleCommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     * @since 18/04/2017
     */
    @Override
    public List<UserRoleCommunityEntity> findAllByFieldUserIdRoleId(Long userId, Long roleId) {
        Query query = this.getEntityManager().createQuery("SELECT m FROM UserRoleCommunityEntity as m WHERE m.user.id = :userId AND m.role.id = :roleId");
        query.setParameter("userId", userId);
        query.setParameter("roleId", roleId);
        return query.getResultList();
    }

    /**
     * description
     *
     * @param communityId
     * @param userId
     * @param roleId
     * @return UserRoleCommunityEntity
     * @author PosmaGroup
     * @version 1.0
     * @since 18/04/2017
     */
    @Override
    public UserRoleCommunityEntity findAllByFieldCommunityIdUserIdRoleId(Long communityId, Long userId, Long roleId) {
        Query query = this.getEntityManager().createQuery("SELECT m FROM UserRoleCommunityEntity as m " +
                "WHERE m.user.id = :userId " +
                "AND m.role.id = :roleId " +
                "AND m.community.id = :communityId");
        query.setParameter("userId", userId);
        query.setParameter("roleId", roleId);
        query.setParameter("communityId", communityId);

        return (UserRoleCommunityEntity) query.getSingleResult();
    }


    /**
     * description
     *
     * @param fieldName
     * @param id
     * @param secondFieldName
     * @param id2
     * @return List<UserRoleCommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     * @since 24/04/2017
     */
    @Override
    public List<UserRoleCommunityEntity> findAllByTwoFieldsId(String fieldName, long id, String secondFieldName, long id2) {
        return entityManager.createQuery("from UserRoleCommunityEntity where " + fieldName + "=" + id + " " +
                "AND " + secondFieldName + "=" + id2 + " " +
                "AND STATUS = " + Status.ACTIVE.ordinal()).getResultList();
    }


    /**
     * description
     *
     * @param communityId
     * @param sectionId
     * @return List<UserEntity>
     * @author PosmaGroup
     * @version 1.0
     * @since 24/04/2017
     */
    @Override
    @Transactional
    public List<UserEntity> findAllMinusSection(long communityId, long sectionId) {

        // consulta todos los estudiantes de la comunidad
        Query query = this.getEntityManager().createQuery("SELECT m FROM UserEntity as m, UserRoleCommunityEntity urc " +
                "WHERE m.id = urc.user.id " +
                "AND urc.role.id = 2 " +
                "AND urc.community.id = :communityId");
        query.setParameter("communityId", communityId);


        List<UserEntity> userList = query.getResultList();

        //Consulta todos los estudiantes de una seccion
        query = this.getEntityManager().createQuery("SELECT m FROM SectionEntity as m " +
                "WHERE m.id = :sectionId " +
                "AND STATUS = " + Status.ACTIVE.ordinal());
        query.setParameter("sectionId", sectionId);

        SectionEntity sectionEntity = (SectionEntity) query.getSingleResult();

        // remove student from community sutudent list
        for (UserEntity userEntity : sectionEntity.getStudentList()) {
            userList.remove(userEntity);
        }

        return userList;
    }

}