package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 11/04/2017.
 */
@Entity
@Table(name = "EVALUATION_TOOL")
public class EvaluationToolEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "EVALUATION_TOOL_SEQ", sequenceName = "S_ID_EVALUATION_TOOL", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVALUATION_TOOL_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=false, length = 50)
    private String name;

    @Column(name = "DESCRIPTION", nullable=true, length = 512)
    private String description;


    public EvaluationToolEntity() {

    }

    public EvaluationToolEntity(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
