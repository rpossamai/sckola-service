package com.posma.sckola.app.persistence.Trigger;

import com.posma.sckola.app.persistence.dao.EvaluationPlanDao;
import com.posma.sckola.app.persistence.entity.EvaluationPlanEntity;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.PrePersist;

/**
 * @descripcion realiza las validaciones de los atributos de EvaluationPlan
 * @author Francis Ries
 * @version 1.1 26/04/2017
 */

public class EvaluationPlanTrig {

    @Autowired
    EvaluationPlanDao evaluationPlanDao;

    @PrePersist
    public void validateEvaluationPlan(EvaluationPlanEntity evaluationPlanEntity) throws Exception{

        if (evaluationPlanEntity.getName() == null)
            throw new Exception("2#El nombre de Plan de Evaluacion es obligatorio");

        if(evaluationPlanEntity.getUser() == null)
            throw new Exception("2#Plan de Evaluacion debe tener el id de quién lo crea");

    }

}
