package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.SectionDao;
import com.posma.sckola.app.persistence.entity.SectionEntity;
import com.posma.sckola.app.persistence.entity.StatusSection;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Francis on 21/04/2017.
 */

@Repository("SectionDao")
public class SectionDaoImpl extends AbstractDao<SectionEntity> implements SectionDao {

    public SectionDaoImpl(){
        super ();
        setClazz(SectionEntity.class);
    }

    /**
     * description
     * @since 24/04/2017
     * @return List<SectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    @Override
    public List<SectionEntity> findAllByCommunity(long communityId, StatusSection status){
        Query query = this.getEntityManager().createQuery("SELECT m FROM SectionEntity as m WHERE " +
                "m.community.id = :communityId " +
                "AND m.status = :status " +
                "ORDER BY m.name");
        query.setParameter("communityId", communityId);
        query.setParameter("status", status);
        return query.getResultList();
    }

    @Override
    public List<SectionEntity> findAllByMatterCommunity(Long matterId, Long communityId) {
        Query query =  this.getEntityManager().createQuery("SELECT DISTINCT s FROM SectionEntity s, MatterCommunitySectionEntity  mcs, MatterCommunityEntity mc " +
                "WHERE s = mcs.section " +
                "AND s.community.id = :communityId " +
                "AND mcs.matterCommunity.matter.id = :matterId");

        query.setParameter("matterId",matterId);
        query.setParameter("communityId", communityId);

        return query.getResultList();
    }

}
