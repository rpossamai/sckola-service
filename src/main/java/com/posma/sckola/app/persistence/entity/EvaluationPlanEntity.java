package com.posma.sckola.app.persistence.entity;

import com.posma.sckola.app.persistence.Trigger.EvaluationPlanTrig;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 11/04/2017.
 */
@Entity
@Table(name = "EVALUATION_PLAN")
@EntityListeners({EvaluationPlanTrig.class})
public class EvaluationPlanEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "EVALUATION_PLAN_SEQ", sequenceName = "S_ID_EVALUATION_PLAN", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVALUATION_PLAN_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=false, length = 80)
    private String name;

    @OneToMany(mappedBy="evaluationPlan", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<EvaluationEntity> evaluationList = new ArrayList<EvaluationEntity>();

    @OneToOne(mappedBy = "evaluationPlan", cascade = CascadeType.ALL)
    @JoinColumn(columnDefinition = "MATTER_COMMUNITY_SECTION_ID")
    private MatterCommunitySectionEntity matterCommunitySection;

    @Column(name = "STATUS", nullable = false)
    private Status status = Status.ACTIVE;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private UserEntity user;



    public EvaluationPlanEntity() {

    }

    public EvaluationPlanEntity(String name, UserEntity user) {
        this.name = name;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EvaluationEntity> getEvaluationList() {
        return evaluationList;
    }

    public void setEvaluationList(List<EvaluationEntity> evaluationList) {
        this.evaluationList = evaluationList;
    }

    public boolean addEvaluationList(EvaluationEntity evaluation) {
        return evaluationList.add(evaluation);
    }

    public MatterCommunitySectionEntity getMatter() {
        return matterCommunitySection;
    }

    public void setMatter(MatterCommunitySectionEntity matterCommunitySection) {
        this.matterCommunitySection = matterCommunitySection;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }


}
