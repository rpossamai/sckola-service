package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.EvaluationToolTemplateDao;
import com.posma.sckola.app.persistence.entity.EvaluationToolTemplateEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 27/04/2017.
 */

@Repository("EvaluationToolTemplateDao")
public class EvaluationToolTemplateDaoImpl extends AbstractDao<EvaluationToolTemplateEntity> implements EvaluationToolTemplateDao {

    public EvaluationToolTemplateDaoImpl(){
        super ();
        setClazz(EvaluationToolTemplateEntity.class);
    }


}
