package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.QualificationUserEntity;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface QualificationUserDao {

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @return QualificationUserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    QualificationUserEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<QualificationUserEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<QualificationUserEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @param qualificationUser
     * @return QualificationUserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    QualificationUserEntity create(QualificationUserEntity qualificationUser);

    /**
     * description
     * @since 12/04/2017
     * @param qualificationUser
     * @return QualificationUserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    QualificationUserEntity update(QualificationUserEntity qualificationUser);

    /**
     * description
     * @since 12/04/2017
     * @param qualificationUser
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(QualificationUserEntity qualificationUser);

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

    List<QualificationUserEntity> GetAllToStudent(Long evaluationPlanId, Long studentId);

    QualificationUserEntity getToStudentEvaluation(Long evaluationId, Long studentId);

    List<QualificationUserEntity> getAllToEvaluation(Long evaluationId);
}
