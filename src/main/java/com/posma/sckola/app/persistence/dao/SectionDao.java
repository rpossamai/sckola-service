package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.SectionEntity;
import com.posma.sckola.app.persistence.entity.StatusSection;

import java.util.List;

/**
 * Created by Francis Ries on 21/04/2017.
 */
public interface SectionDao {

    /**
     * description
     * @since 21/04/2017
     * @param id evaluation identifier
     * @return SectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    SectionEntity findOne(long id);

    /**
     * description
     * @since 21/04/2017
     * @return List<SectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<SectionEntity> findAll();

    /**
     * description
     * @since 21/04/2017
     * @return List<SectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<SectionEntity> findAllByFieldId(String fieldName, long id);


    /**
     * description
     * @since 21/04/2017
     * @param section
     * @return SectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    SectionEntity create(SectionEntity section);

    /**
     * description
     * @since 21/04/2017
     * @param section
     * @return SectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    SectionEntity update(SectionEntity section);

    /**
     * description
     * @since 21/04/2017
     * @param section
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(SectionEntity section);

    /**
     * description
     * @since 21/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);


    /**
     * description list all sections from community id
     * @since 24/04/2017
     * @return List<MatterCommunitySectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    public List<SectionEntity> findAllByCommunity(long communityId, StatusSection status);

    /**
     * Obtiene la lista de las secciones de una materia que pertenecen a una comunidad
     * @param matterId
     * @param communityId
     * @return
     */
    List<SectionEntity> findAllByMatterCommunity(Long matterId, Long communityId);
}
