package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Posma-dev on 22/08/2017.
 */
@Entity
@Table(name="NOTIFICATION")
public class NotificationEntity implements Serializable {


    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "NOTIFICATION_SEQ", sequenceName = "S_ID_NOTIFICATION", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NOTIFICATION_SEQ")
    private Long id;


    @Column(name = "TYPE_NAME", nullable=false)
    private String typeName;

    @Column(name = "TEXT", nullable=false)
    private String text;

    @Column(name = "DATE", nullable=false)
    private Date date;

    @Column(name = "ACTIVE", nullable=false)
    private boolean active;


    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private NotificationType type;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private NotificationStatus status;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "EVALUATION_ID", referencedColumnName = "ID", nullable = true)
    private EvaluationEntity evaluation;

    @ManyToOne
    @JoinColumn(name="MATTER_COMMUNITY_SECTION_ID", referencedColumnName = "ID", nullable = false)
    private MatterCommunitySectionEntity matter;

    @ManyToOne
    @JoinColumn(name="COMMUNITY_ID",referencedColumnName = "ID",nullable = false)
    private CommunityEntity community;

    @ManyToOne
    @JoinColumn(name="SECTION_ID",referencedColumnName = "ID",nullable = false)
    private SectionEntity section;

    public NotificationEntity() {
    }

    public NotificationEntity(String typeName, String text, NotificationType type, UserEntity user, MatterCommunitySectionEntity matter, CommunityEntity community) {
        this.typeName = typeName;
        this.text = text;
        this.type = type;
        this.user = user;
        this.matter = matter;
        this.community = community;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public MatterCommunitySectionEntity getMatter() {
        return matter;
    }

    public void setMatter(MatterCommunitySectionEntity matter) {
        this.matter = matter;
    }

    public CommunityEntity getCommunity() {
        return community;
    }

    public void setCommunity(CommunityEntity community) {
        this.community = community;
    }

    public NotificationStatus getStatus() {
        return status;
    }

    public void setStatus(NotificationStatus status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public SectionEntity getSection() {
        return section;
    }

    public void setSection(SectionEntity section) {
        this.section = section;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public EvaluationEntity getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(EvaluationEntity evaluation) {
        this.evaluation = evaluation;
    }
}
