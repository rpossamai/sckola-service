package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Francis Ries on 12/04/2017.
 */
@Entity
@Table(name = "CURRICULUM")
public class CurriculumEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "CURRICULUM_SEQ", sequenceName = "S_ID_CURRICULUM", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CURRICULUM_SEQ")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
    private UserEntity user;

    @Column(name = "TITLE", nullable=false)
    private String title;

    @ManyToOne
    @JoinColumn(name = "COMMUNITY_ID", referencedColumnName = "ID", nullable = true)
    private CommunityEntity community;

    @Column(name = "INSTITUTE", nullable=false)
    private String institute;

    @Column(name = "DATE", nullable=true)
    @Temporal(TemporalType.DATE)
    private Date date;

    
    public CurriculumEntity() {

    }

    public CurriculumEntity(UserEntity user, String title, CommunityEntity community, Date date) {
        this.user = user;
        this.title = title;
        this.community = community;
        this.date = date;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public CommunityEntity getCommunity() {
        return community;
    }

    public void setCommunity(CommunityEntity community) {
        this.community = community;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
