package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 27/03/2017.
 */

@Entity
@Table(name = "PHONE")
public class PhoneEntity implements Serializable {


    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name="ability_seq", sequenceName="s_id_ability", initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ability_seq")
    private Long id;

    @ManyToOne(targetEntity = OrganizationEntity.class)
    @JoinColumn(referencedColumnName = "ID",name = "ORGANIZATION_ID", nullable = false)
    private OrganizationEntity organization;

    @Column(name = "PHONE", nullable = false)
    private String phone;

    public PhoneEntity(){}

    public PhoneEntity(String phone){this.phone = phone;}

    public Long getId(){return id;}

    public void setId(Long id) {this.id = id;}

    public String getPhone(){return phone;}

    public void setPhone(String phone){ this.phone = phone;}

    public OrganizationEntity getOrganization(){return organization;}

    public void setOrganization(OrganizationEntity organization){ this.organization = organization;}
}
