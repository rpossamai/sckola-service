package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 11/04/2017.
 */
@Entity
@Table(name = "MATTER",
        indexes = {
                @Index(columnList ="NAME",name = "IDX_NAME_MATTER")
        })
public class MatterEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "MATTER_SEQ", sequenceName = "S_ID_MATTER", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MATTER_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=false)
    private String name;
    
    @Column(name = "DESCRIPTION", nullable=true, length = 1024)
    private String description;

    @Column(name = "OBJECTIVE", nullable=true, length = 1024)
    private String objective;

    @Column(name = "ESPECIFIC_OBJECTIVE", nullable=true, columnDefinition = "text")
    private String especificObjective;


    public MatterEntity() {

    }

    public MatterEntity(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getEspecificObjective() {
        return especificObjective;
    }

    public void setEspecificObjective(String especificObjective) {
        this.especificObjective = especificObjective;
    }

}
