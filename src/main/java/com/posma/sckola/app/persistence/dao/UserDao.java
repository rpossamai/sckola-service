package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.StatusUser;
import com.posma.sckola.app.persistence.entity.UserEntity;

import java.util.List;

/**
 * Created by Francis on 27/03/2017.
 */
public interface UserDao {

    /**
     * description 
     * @since 27/03/2017
     * @param id user identifier
     * @return UserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    UserEntity findOne(long id);
    
    /**
     * description 
     * @since 27/03/2017
     * @return List<UserEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<UserEntity> findAll();

    /**
     * description 
     * @since 27/03/2017
     * @param fieldName column name
     * @param id user identifier
     * @return List<UserEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<UserEntity> findAllByFieldId(String fieldName, long id);

    /**
     * description
     * @since 27/03/2017
     * @param fieldName column name
     * @param id user identifier
     * @return List<UserEntity>
     * @author Francis Ries
     * @version 1.0
     */
    List<UserEntity> findAllByField(String fieldName, Object id);

    /**
     * description
     * @since 27/03/2017
     * @param status
     * @return List<UserEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<UserEntity> findAllByFieldStatus(StatusUser status);

    /**
     * description 
     * @since 27/03/2017
     * @param user
     * @return UserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    UserEntity create(UserEntity user);

    /**
     * description 
     * @since 27/03/2017
     * @param user
     * @return UserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    UserEntity update(UserEntity user);

    /**
     * description 
     * @since 27/03/2017
     * @param user
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(UserEntity user);

    /**
     * description
     * @since 27/03/2017
     * @param id user identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);


    /**
     * Search user by mail (username)
     * @since 27/03/2017
     * @param mail
     * @author Francis Ries
     * @version 1.0
     */
    UserEntity findByMail(String mail);


}
