package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.ConfMailDao;
import com.posma.sckola.app.persistence.entity.ConfMailEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 27/03/2017.
 */

@Repository("ConfMailDao")
public class ConfConfMailDaoImpl extends AbstractDao<ConfMailEntity> implements ConfMailDao {

    public ConfConfMailDaoImpl(){
        super ();
        setClazz(ConfMailEntity.class);
    }


}
