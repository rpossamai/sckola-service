package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.UserEntity;
import com.posma.sckola.app.persistence.entity.UserRoleCommunityEntity;

import java.util.List;

/**
 * Created by Francis Ries on 18/04/2017.
 */
public interface UserRoleCommunityDao {

    /**
     * description
     * @since 18/04/2017
     * @param id userRoleCommunity identifier
     * @return UserRoleCommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    UserRoleCommunityEntity findOne(long id);

    /**
     * description
     * @since 18/04/2017
     * @return List<UserRoleCommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<UserRoleCommunityEntity> findAll();

    /**
     * description
     * @since 18/04/2017
     * @param userRoleCommunity
     * @return UserRoleCommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    UserRoleCommunityEntity create(UserRoleCommunityEntity userRoleCommunity);

    /**
     * description
     * @since 18/04/2017
     * @param userRoleCommunity
     * @return UserRoleCommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    UserRoleCommunityEntity update(UserRoleCommunityEntity userRoleCommunity);

    /**
     * description
     * @since 18/04/2017
     * @param userRoleCommunity
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(UserRoleCommunityEntity userRoleCommunity);

    /**
     * description
     * @since 18/04/2017
     * @param id userRoleCommunity identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);


    /**
     * description
     * @since 18/04/2017
     * @param fieldName
     * @param id
     * @param secondFieldName
     * @param id2
     * @return List<UserRoleCommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<UserRoleCommunityEntity> findAllByTwoFieldsId(String fieldName, long id, String secondFieldName, long id2);


    /**
     * description
     * @since 18/04/2017
     * @param userId
     * @param roleId
     * @return List<UserRoleCommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<UserRoleCommunityEntity> findAllByFieldUserIdRoleId(Long userId, Long roleId);


    /**
     * description
     * @since 18/04/2017
     * @param communityId
     * @param userId
     * @param roleId
     * @return UserRoleCommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    UserRoleCommunityEntity findAllByFieldCommunityIdUserIdRoleId( Long communityId, Long userId, Long roleId);

    /**
     * description
     * @since 24/04/2017
     * @param communityId
     * @param sectionId
     * @return List<UserEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<UserEntity> findAllMinusSection(long communityId, long sectionId);

}
