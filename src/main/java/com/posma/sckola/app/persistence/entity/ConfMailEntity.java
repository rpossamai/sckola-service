package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis on 27/03/2017.
 */

@Entity
@Table(name = "CONF_MAIL")
@NamedQueries({
        @NamedQuery(
                name = ConfMailEntity.FIND_BY_ID,
                query = "select u from ConfMailEntity u where id = :id"
        ),
        @NamedQuery(
                name = ConfMailEntity.DELETE_BY_ID,
                query = "delete from ConfMailEntity where id = :id"
        )
})
public class ConfMailEntity implements Serializable {

    public static final String FIND_BY_ID = "confMailEntity.findById";
    public static final String FIND_ALL = "confMailEntity.findAll";
    public static final String DELETE_BY_ID = "confMailEntity.deleteById";

    @Id
    @SequenceGenerator(name="mail_seq", sequenceName="s_id_config_mail")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mail_seq")
    private Long id;

    @Column(name = "USERNAME", nullable=false, length = 40)
    public String userName;

    @Column(name = "PASSWORD", nullable=false, length = 20)
    public String password;

    @Column(name = "PROTOCOL", nullable=false, length = 20)
    public String protocol;

    @Column(name = "PORT", nullable=false)
    public Long port;

    @Column(name = "EMAIL", nullable=false, length = 50)
    public String email;

    @Column(name = "HOST_SMTP", nullable=false, length = 30)
    public String hostSMTP;

    public ConfMailEntity() {

    }

    public ConfMailEntity(String userName,String protocol, Long port, String email, String password, String hostSMTP) {
        this.userName = userName;
        this.protocol = protocol;
        this.port = port;
        this.email = email;
        this.password = password;
        this.hostSMTP = hostSMTP;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUserName(String userName){this.userName=userName; }

    public String getUserName(){return userName;}

    public void setProtocol(String protocol){this.protocol=protocol;}

    public String getProtocol(){return protocol;}

    public void setPort(Long port){this.port=port;}

    public Long getPort(){return port;}

    public void setEmail(String email){this.email=email;}

    public String getEmail(){return email;}

    public void setPassword(String password){this.password=password;}

    public String getPassword(){return password;}

    public String getHostSMTP() {
        return hostSMTP;
    }

    public void setHostSMTP(String hostSMTP) {
        this.hostSMTP = hostSMTP;
    }

}
