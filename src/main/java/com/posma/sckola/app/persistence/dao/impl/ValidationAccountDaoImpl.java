package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.ValidationAccountDao;
import com.posma.sckola.app.persistence.entity.ValidationAccountEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

/**
 * Created by Francis on 27/03/2017.
 */
@Repository("ValidationAccountDao")
public class ValidationAccountDaoImpl extends AbstractDao<ValidationAccountEntity> implements ValidationAccountDao {

    public ValidationAccountDaoImpl(){
        super ();
        setClazz(ValidationAccountEntity.class);
    }

    /**
     * Busca el usuario con el codigo de validacion especificado
     * @since 27/03/2017
     * @param validationCode
     * @return ValidationAccountEntity
     * @author Francis Ries
     * @version 1.0
     */
    public ValidationAccountEntity findByValidationCode(String validationCode) {
        Query query = this.getEntityManager().createQuery("SELECT m FROM ValidationAccountEntity as m where m.validationCode ='" + validationCode + "'", ValidationAccountEntity.class);
        return (ValidationAccountEntity)query.getSingleResult();
    }



}
