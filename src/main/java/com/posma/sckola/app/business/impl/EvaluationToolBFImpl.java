package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.EvaluationToolBF;
import com.posma.sckola.app.dto.*;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 25/04/2017.
 */
@Service
public class EvaluationToolBFImpl implements EvaluationToolBF {

    @Autowired
    EvaluationToolDao evaluationToolDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    /**
     * Service query all evaluation tools
     * @since 25/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    public MessageDto getAll() {

        MessageDto messageDto = new MessageDto();

        List<EvaluationToolEntity> evaluationToolEntityList = evaluationToolDao.findAll();
        List<EvaluationToolDto> evaluationToolDtoList = new ArrayList<EvaluationToolDto>(evaluationToolEntityList.size());

        for(EvaluationToolEntity evaluationToolEntity:evaluationToolEntityList){
            evaluationToolDtoList.add(entityMapper.entityToBO(evaluationToolEntity));
        }

        messageDto.setResponse(evaluationToolDtoList);

        return messageDto;
    }

}

