package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.SectionBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.SectionDto;
import com.posma.sckola.app.dto.UserDto;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 20/04/2017.
 */
@Service
public class SectionBFImpl implements SectionBF {


    @Autowired
    SectionDao sectionDao;

    @Autowired
    WizardDao wizardDao;

    @Autowired
    UserDao userDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    CommunityDao communityDao;
    

    /**
     * Service query all sections from a community with status "ACTIVE"
     * @since 24/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto sectionCommunityGetAll(Long communityId){

        MessageDto messageDto = new MessageDto();

        List<SectionEntity> SectionEntityList = sectionDao.findAllByCommunity(communityId.longValue(), StatusSection.ACTIVE);
        List<SectionDto> sectionDtoList = new ArrayList<>(SectionEntityList.size());

        for(SectionEntity sectionEntity:SectionEntityList){
            sectionDtoList.add(entityMapper.entityToBO(sectionEntity));
        }

        messageDto.setResponse(sectionDtoList);

        return messageDto;
    }


    /**
     * Service create section in a community
     * @since 24/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto sectionCommunityCreate(Long communityId, SectionDto sectionDto, Long idUser){

        WizardEntity wizardEntity;
        MessageDto messageDto = new MessageDto();
        List<WizardEntity> wizardEntityList = wizardDao.findAllByFieldId("USER_ID", idUser);

        try{
            CommunityEntity communityEntity = communityDao.findOne(communityId);

            if(communityEntity == null || communityEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Community not exists");
                return messageDto;
            }

            List<SectionDto> sectionDtoList = (List<SectionDto>)sectionCommunityGetAll(communityId).getResponse();

            for(SectionDto sectionDtoAux: sectionDtoList){
                //valida que no exista una seccion con el mismo nombre con status "ACTIVE"
                if(sectionDtoAux.getName().equalsIgnoreCase(sectionDto.getName().trim().toUpperCase())){
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("SK-005");
                    messageDto.setMessage(systemMessage.getMessage("SK-005"));
                    return messageDto;
                }
            }

            SectionEntity sectionEntity = new SectionEntity(sectionDto.getName().trim().toUpperCase(),
                    communityEntity,
                    StatusSection.ACTIVE);

            sectionEntity = sectionDao.create(sectionEntity);

            if(sectionEntity != null && sectionEntity.getId() != null) {
                messageDto.setResponse(entityMapper.entityToBO(sectionEntity));
                if(wizardEntityList.size() >= 1) {
                    wizardEntity = wizardEntityList.get(0);
                    long idSection = sectionEntity.getId();
                    MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(idSection);
                    wizardEntity.setMatterCommunitySection(matterCommunitySectionEntity);
                    wizardEntity = wizardDao.update(wizardEntity);
                    entityMapper.entityToBO(wizardEntity);
                }

            }else{
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter, Community or User not exists");
        }

        return messageDto;
    }


    /**
     * Consultar el listado de estudiantes de una seccion
     * @since 25/04/2017
     * @param sectionId section identifier
     * @author FrancisRies
     * @version 1.0
     */

    @Override
    @Transactional
    public MessageDto getAllUserSection(Long sectionId){MessageDto messageDto = new MessageDto();

        List<UserDto> userDtoList = new ArrayList<UserDto>();

        try {
            SectionEntity sectionEntity = sectionDao.findOne(sectionId);

            if(sectionEntity == null || sectionEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter or Community not exists");
                return messageDto;
            }

            for (UserEntity userEntity: sectionEntity.getStudentList()) {
                userDtoList.add(entityMapper.entityToBO(userEntity));
            }
            messageDto.setResponse(userDtoList);


        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("GET-COM-USER-001");
            messageDto.setMessage(systemMessage.getErrorCode().get("GET-COM-USER-001"));
        }

        return messageDto;
    }

    /**
     * Add user to a section
     * @since 25/04/2017
     * @param userId user identifier
     * @param sectionId section identifier
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto associateUserSection(Long userId, Long sectionId){
        MessageDto messageDto = new MessageDto();

        try {
            SectionEntity sectionEntity = sectionDao.findOne(sectionId);

            UserEntity userEntity = userDao.findOne(userId);

            if(sectionEntity == null || sectionEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - User or Section not exists");
                return messageDto;
            }

            sectionEntity.addStudentList(userEntity);
            sectionEntity = sectionDao.update(sectionEntity);

            if(sectionEntity != null || sectionEntity.getId() != null){
                messageDto.setIdRequest(userId);
                messageDto.setResponse(entityMapper.entityToBO(sectionEntity));
                return messageDto;
            }

        }catch (DataIntegrityViolationException cve){
            cve.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }

        messageDto.setSuccess(false);
        messageDto.setErrorCode("SK-001");
        messageDto.setMessage(systemMessage.getErrorCode().get("SK-001"));

        return messageDto;
    }


    /**
     * remove user from a section
     * @since 25/04/2017
     * @param userId user identifier
     * @param sectionId section identifier
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto desassociateUserSection(Long userId, Long sectionId){
        MessageDto messageDto = new MessageDto();

        try {
            SectionEntity sectionEntity = sectionDao.findOne(sectionId);

            UserEntity userEntity = userDao.findOne(userId);

            if(sectionEntity == null || sectionEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - User or Section not exists");
                return messageDto;
            }

            sectionEntity.removeStudentList(userEntity);
            sectionEntity = sectionDao.update(sectionEntity);

            if(sectionEntity != null || sectionEntity.getId() != null){
                messageDto.setIdRequest(userId);
                messageDto.setResponse(entityMapper.entityToBO(sectionEntity));
                return messageDto;
            }

        }catch (DataIntegrityViolationException cve){
            cve.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }

        messageDto.setSuccess(false);
        messageDto.setErrorCode("SK-001");
        messageDto.setMessage(systemMessage.getErrorCode().get("SK-001"));

        return messageDto;
    }
}

