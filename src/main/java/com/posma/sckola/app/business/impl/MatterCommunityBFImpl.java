package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.MatterCommunityBF;
import com.posma.sckola.app.dto.*;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by Francis Ries on 20/04/2017.
 */
@Service
public class MatterCommunityBFImpl implements MatterCommunityBF {

    @Autowired
    MatterDao matterDao;

    @Autowired
    SectionDao sectionDao;

    @Autowired
    WizardDao wizardDao;

    @Autowired
    UserDao userDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    CommunityDao communityDao;

    @Autowired
    MatterCommunityDao matterCommunityDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    /**
     * Service query all Matters from a community
     * @since 20/04/2017
     * @author FrancisRies
     * @version 1.0
     */
   /* @Override
    public MessageDto matterGetAllByCommunity(Long communityId){

        MessageDto messageDto = new MessageDto();

        // buscar la materia en la comunidad a ver si ya existe
        List<MatterCommunityEntity> matterCommunityEntityList = matterCommunityDao.findAllByFieldId("COMMUNITY_ID", communityId.longValue());
        List<MatterCommunityDto> matterCommunityDtoList = new ArrayList<MatterCommunityDto>(matterCommunityEntityList.size());

        for(MatterCommunityEntity matterCommunityEntity:matterCommunityEntityList){
            matterCommunityDtoList.add(entityMapper.entityToBO(matterCommunityEntity));
        }

        messageDto.setResponse(matterCommunityDtoList);

        return messageDto;
    }

*/

    /**
     * Service get detall from Matters
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    public MessageDto matterCommunityGetById(Long matterCommunityId){

        MessageDto messageDto = new MessageDto();
        MatterCommunityEntity matterCommunityEntity;

        try{
            matterCommunityEntity = matterCommunityDao.findOne(matterCommunityId.longValue());

            if(matterCommunityEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter not exists");
                return messageDto;
            }

            messageDto.setResponse(entityMapper.entityToBO(matterCommunityEntity));
            return messageDto;

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter not exists");
            return messageDto;
        }
    }



    /**
     * creea la clase (relacion Materia, seccion, profesor, sectionType)
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto matterCommunitySectionCreate(Long matterCommunityId, Long sectionId, Long userId, SectionTypeDto sectionType){

        MessageDto messageDto = new MessageDto();

        MatterCommunityEntity matterCommunityEntity;
        SectionEntity sectionEntity;
        UserEntity userEntity;
        WizardEntity wizardEntity;
        List<WizardEntity> wizardEntityList = wizardDao.findAllByFieldId("USER_ID", userId);

        try{
            matterCommunityEntity = matterCommunityDao.findOne(matterCommunityId);
            sectionEntity = sectionDao.findOne(sectionId);
            userEntity = userDao.findOne(userId);

            if(matterCommunityEntity == null || sectionEntity == null || userEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter, Community or User not exists");
                return messageDto;
            }

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter, Community or User not exists");
            return messageDto;
        }

        // valida si ya existe la class con status ACTIVE
        MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findMatterCommunitySectionUser(matterCommunityId, sectionId, userId, StatusClass.ACTIVE);

        // mensaje de error porque ya existe la relacion
        if(matterCommunitySectionEntity != null && matterCommunitySectionEntity.getId() != null){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-005");
            messageDto.setMessage(systemMessage.getMessage("SK-005"));
            return messageDto;
        }

        matterCommunitySectionEntity = matterCommunitySectionDao.create(
                new MatterCommunitySectionEntity(matterCommunityEntity,sectionEntity,userEntity, StatusClass.ACTIVE, SectionType.valueOf(sectionType.getType())));

        if(sectionEntity != null && sectionEntity.getId() != null) {
            messageDto.setResponse(entityMapper.entityToBO(sectionEntity));
            if(wizardEntityList.size() >= 1) {
                wizardEntity = wizardEntityList.get(0);
                long idSection = sectionEntity.getId();
             //   MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(idSection);
                wizardEntity.setMatterCommunitySection(matterCommunitySectionEntity);
                wizardEntity = wizardDao.update(wizardEntity);
                entityMapper.entityToBO(wizardEntity);
            }

        }else{
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-001");
            messageDto.setMessage(systemMessage.getMessage("SK-001"));
        }


        if(matterCommunitySectionEntity != null && matterCommunitySectionEntity.getId() != null) {
            messageDto.setResponse(entityMapper.entityToBO(matterCommunitySectionEntity));
        }else{
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-001");
            messageDto.setMessage(systemMessage.getMessage("SK-001"));
        }

        return messageDto;
    }



}

