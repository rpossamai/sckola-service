package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.LoginDto;
import com.posma.sckola.app.dto.MessageDto;

/**
 * Created by Francis on 27/03/2017.
 */
public interface LoginBF {

    /**
     * validar inicio de session en el systema
     * @since 27/03/2017
     * @return identifier login
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto login(LoginDto loginDto);


}


