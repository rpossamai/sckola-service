package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.RequestAssociateDto;

/**
 * Created by Posma-ricardo on 01/06/2017.
 */
public interface MatterCommunitySectionBF {

    MessageDto matterCommunitySectionDesassociate(Long matterCommunitySectionId, RequestAssociateDto requestAssociateDto);
}
