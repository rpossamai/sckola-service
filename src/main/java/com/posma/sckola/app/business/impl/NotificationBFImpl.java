package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.dto.*;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.persistence.NoResultException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Francis on 16/02/2017.
 */
@Service
@EnableScheduling
public class NotificationBFImpl implements NotificationBF {

    @Autowired
    ConfMailDao confMailDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    AssistanceByMatterDao assistanceByMatterDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    EvaluationDao evaluationDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    Validation validation;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    UserDao userDao;

    /**
     * Service that query all mail request
     *
     * @return NotificationDto
     * @author FrancisRies
     * @version 1.0
     * @since 16/02/2017
     */
    @Override
    @Transactional
    public Boolean sendNotification(NotificationDto notificationDto) {
        ConfMailEntity mailEntity = confMailDao.findOne(1);
        boolean tokenSendMail = true;
        if (!sendMail(notificationDto.getTo(), notificationDto.getTitle(), notificationDto.getText())) {
            if (!sendMail(notificationDto.getTo(), notificationDto.getTitle(), notificationDto.getText())) {
                if (!sendMail(notificationDto.getTo(), notificationDto.getTitle(), notificationDto.getText())) {
                    // no fue posible enviar el correo luego de tres intentos
                    // cambiar esto para un been que crea procesos por cada solicitud y que en caso de falla duerme 1min y vuelve a intentar, luevo 2 min y vuelve a tratar,
                    // en unltima instacia notifica la falla a un proceso que manejara las fallas de correo, para que el usuario pueda volver a enviar la invitacion
                    tokenSendMail = false;
                }
            }
        }
        ;

        return new Boolean(tokenSendMail);
    }

    /**
     * Observer for notifications in active users
     */
    @Override
    @Transactional
    @Scheduled(fixedDelay = 30000)
    public void newNotifications() {
        List<UserEntity> activeUsers = userDao.findAllByFieldStatus(StatusUser.ACTIVE);
        for (UserEntity user : activeUsers) {
            updateNotificationsByUser(user);
        }
    }


    /**
     * Used to update the status of the notification to SENDED.
     *
     * @param notification notification to change status.
     * @return boolean
     */
    @Override
    @Transactional
    public boolean updateNotificationStatus(DashboardNotificationDto notification) {


        NotificationEntity notificationUpdate = notificationDao.findOne(notification.getId());
        notificationUpdate.setStatus(NotificationStatus.SENDED);
        notificationDao.update(notificationUpdate);
        return true;
    }

    /**
     * Service to get notifications in database.
     *
     * @return List with all the notifications to send.
     */
    @Override
    public List<DashboardNotificationDto> getNotifications() {

        List<UserEntity> activeUsers = userDao.findAllByFieldStatus(StatusUser.ACTIVE);
        List<NotificationEntity> notificationsDB = new ArrayList<NotificationEntity>();
        for (UserEntity user : activeUsers) {
            notificationsDB.addAll(notificationDao.findByUser(user.getMail()));
        }
        return processToResult(notificationsDB);
    }

    /**
     * Gets history notifications for a user in a community
     * @param userMail email of the user to find history
     * @param communityId id of the community
     * @return return history notifications and active notifications actually
     */
    @Override
    public MessageDto getHistoryNotificationByUser(String userMail, Long communityId){
        MessageDto messageDto = new MessageDto();
        try{
            List<NotificationEntity> notifications = new ArrayList<>(notificationDao.findHistoryNotification(userMail,communityId));
            List<DashboardNotificationDto> notificationsResult = processToResult(notifications);
            NotificationHistoryDto history = new NotificationHistoryDto();
            history.setNotifications(notificationsResult);
            int activeNotifications = 0;
            for(DashboardNotificationDto notification: notificationsResult){
                if(notification.isActive()){
                    activeNotifications++;
                }
            }
            history.setActiveNotifications(activeNotifications);
            messageDto.setResponse(history);
            return messageDto;
        }
        catch(NoResultException ex){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002") + " - Matter not exists");
            return messageDto;
        }
    }

    /**
     * Service to get all active notifications for an user.
     *
     * @param userMail mail from user to get notifications.
     * @return List with all the active notifications from the given mail.
     */
    @Override
    @Transactional
    public MessageDto getActiveNotificationsByUser(String userMail, Long id) {
        MessageDto messageDto = new MessageDto();
        try {
            List<NotificationEntity> notifications = new ArrayList<>(notificationDao.findActiveNotificationByUserByCommunity(userMail,id));
            messageDto.setResponse(processToResult(notifications));
            return messageDto;
        } catch (NoResultException ex) {
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002") + " - Matter not exists");
            return messageDto;
        }
    }

    /**
     * Sets an active notification to inactive, because the user already attended it.
     *
     * @param id notification id.
     * @return MessageDto with success or error message.
     */
    @Override
    @Transactional
    public MessageDto removeActiveNotification(Long id) {
        MessageDto messageDto = new MessageDto();
        try {
            NotificationEntity notificationUpdate = notificationDao.findOne(id);
            notificationUpdate.setActive(false);
            notificationDao.update(notificationUpdate);
            messageDto.setResponse(true);
            return messageDto;
        } catch (NoResultException ex) {
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002") + " - Matter not exists");
            return messageDto;
        }
    }

    /**
     * Gets value List Dto for an evaluation ratings
     *
     * @param ratings scale of the evaluation
     * @return List with all the values dto
     */
    public List<EvaluationValueDto> getScaleDto(List<RatingValueEntity> ratings) {
        List<EvaluationValueDto> values = new ArrayList<>();
        for (RatingValueEntity qualification : ratings) {
            EvaluationValueDto evaluationValue = new EvaluationValueDto();
            evaluationValue.setId(qualification.getId());
            evaluationValue.setValue(qualification.getQualification().getValue());
            evaluationValue.setText(qualification.getQualification().getText());
            evaluationValue.setWeight(qualification.getQualification().getWeight());
            values.add(evaluationValue);
        }
        return values;
    }

    /**
     * Parse an evaluation entity to EvaluationDto of a notification
     * @param evaluation evaluation that belongs to the notification
     * @return EvaluationDto
     */
    public EvaluationDto parseEvaluation(EvaluationEntity evaluation){
        EvaluationDto evaluationDto = new EvaluationDto();
        evaluationDto.setId(evaluation.getId());
        evaluationDto.setStatus(evaluation.getStatus().name());
        EvaluationScaleDto scale = new EvaluationScaleDto();
        scale.setId(evaluation.getRatingScale().getId());
        scale.setEvaluationValueList(getScaleDto(evaluation.getRatingScale().getRatingValueList()));
        evaluationDto.setEvaluationScale(scale);
        return evaluationDto;
    }

    /**
     * Receives a list of notifications and converts it to its respective data transfer object
     *
     * @param notifications notifications to send
     * @return List of notifications data transfer objects
     */
    @Transactional
    public List<DashboardNotificationDto> processToResult(List<NotificationEntity> notifications) {
        List<DashboardNotificationDto> result = new ArrayList<DashboardNotificationDto>();
        for (NotificationEntity notification : notifications) {
            DashboardNotificationDto notificationResult = new DashboardNotificationDto();
            CommunityDto community = new CommunityDto();
            community.setId(notification.getCommunity().getId());
            community.setName(notification.getCommunity().getName());
            MatterDto matter = new MatterDto();
            matter.setId(notification.getMatter().getId());
            matter.setName(notification.getMatter().getMatterCommunity().getName());
            if (notification.getEvaluation() != null) {
                EvaluationDto evaluationDto;
                if(notification.getEvaluation().getStatus() == StatusEvaluation.PENDING){
                    evaluationDto = parseEvaluation(notification.getEvaluation());
                }
                else{
                    evaluationDto = entityMapper.entityToBO(notification.getEvaluation());
                }
                notificationResult.setEvaluation(evaluationDto);
            }
            SectionDto section = new SectionDto();
            section.setId(notification.getSection().getId());
            section.setName(notification.getSection().getName());
            notificationResult.setId(notification.getId());
            notificationResult.setFrom(notification.getTypeName());
            notificationResult.setText(notification.getText());
            notificationResult.setUsername(notification.getUser().getMail());
            notificationResult.setCommunity(community);
            notificationResult.setMatter(matter);
            notificationResult.setType(notification.getType());
            notificationResult.setSection(section);
            notificationResult.setActive(notification.isActive());
            notificationResult.setDate(notification.getDate().toString());
            result.add(notificationResult);
        }
        return result;
    }

    /**
     * Get all notifications with status NOT_SEND for an specific user.
     *
     * @param user user owner of the notifications.
     */
    public void updateNotificationsByUser(UserEntity user) {

        List<MatterCommunitySectionEntity> teacherMatters = matterCommunitySectionDao.findAllMatterCommunitySectionByTeacher(user.getId(),
                StatusClass.ACTIVE);
        for (MatterCommunitySectionEntity sectionEntity : teacherMatters) {
            AssistanceByMatterPk assistanceByMatterPk = new AssistanceByMatterPk();
            assistanceByMatterPk.setMatterCommunitySection(sectionEntity.getId());
            assistanceByMatterPk.setDate(new Date());
            List<AssistanceByMatterEntity> assistances = assistanceByMatterDao.findAllForSectionForDate(assistanceByMatterPk);

            if (assistances.size() == 0) {
                if(sectionEntity.getSection().getStudentList().size() > 0)
                createNotification(sectionEntity, NotificationType.ASSISTANCE, null);
            }

            if (sectionEntity.getEvaluationPlan() != null) {
                List<EvaluationEntity> evaluations = sectionEntity.getEvaluationPlan().getEvaluationList();
                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date formattedDate = validation.stringToDate(dateFormat.format(date));
                for (EvaluationEntity evaluation : evaluations) {
                    Date evaluationDate = validation.stringToDate(dateFormat.format(evaluation.getDate()));
                    evaluation.getRatingScale().getRatingValueList();
                    evaluation.getId();
                    if (formattedDate.equals(evaluationDate) && sectionEntity.getSection().getStudentList().size() > 0) {
                        createNotification(sectionEntity, NotificationType.SCORE, evaluation);
                    }
                }
            }
        }
    }

    /**
     * Creates a new notification in the database.
     *
     * @param matterCommunitySection Info of the Notification
     * @param type                   Notification type
     * @return
     */
    public NotificationEntity createNotification(MatterCommunitySectionEntity matterCommunitySection, NotificationType type, EvaluationEntity evaluation) {
        NotificationEntity notification = new NotificationEntity();
        if (type == NotificationType.ASSISTANCE) {
            notification.setTypeName("Asistencia");
            notification.setText("Recuerda pasar la asistencia!");
        } else if (type == NotificationType.SCORE) {
            notification.setTypeName("Notas");
            notification.setText("Recuerda almacenar las notas!");
            notification.setEvaluation(evaluation);
        }
        notification.setType(type);
        notification.setUser(matterCommunitySection.getUser());
        notification.setCommunity(matterCommunitySection.getMatterCommunity().getCommunity());
        notification.setMatter(matterCommunitySection);
        notification.setStatus(NotificationStatus.NOT_SENDED);
        notification.setSection(matterCommunitySection.getSection());
        notification.setActive(true);
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        if(calendar.get(calendar.DAY_OF_WEEK) != 1 || calendar.get(calendar.DAY_OF_WEEK) != 7){
            notification.setDate(validation.stringToDate(dateFormat.format(date)));
            if (!notificationExist(notification)) {
            return notificationDao.create(notification);
            } else {
            return notification;
            }
        }else{
            return null;
        }
    }


    /**
     * Checks if a notification already exist
     *
     * @param notification notification to check
     * @return boolean
     */
    public boolean notificationExist(NotificationEntity notification) {

        if (notificationDao.findByNotification(notification).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Send mail to users.
     * @param to_bcc destinataries
     * @param subject subject of the mail
     * @param text content of the mail
     * @return boolean
     */
    private boolean sendMail(String to_bcc, String subject, String text) {
        boolean tokenSendMail = false;

        ConfMailEntity mailEntity = confMailDao.findOne(1);

        Properties props = new Properties();
        //NAME OF MAIL HOST, IN THIS CASE IS: smtp.gmail.com
        props.setProperty("mail.smtp.host", mailEntity.getHostSMTP());  //"smtp.gmail.com"

        //TLS IF ENABLE
        String enable = "true";
        props.setProperty("mail.smtp.starttls.enable", enable);

        //PORT OF GMAIL TO SEND MAILS
        props.setProperty("mail.smtp.port", mailEntity.getPort().toString()); //"587"

        //USER NAME
        props.setProperty("mail.smtp.user", mailEntity.getEmail()); //"posmagroupcompany@gmail.com"

        //USER AND PASWWORD TO CONNECT
        props.setProperty("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);

        MimeMessage message = new MimeMessage(session);

        try {
            //RECIPTS
            message.addRecipients(Message.RecipientType.BCC, to_bcc);

            message.setSubject(subject);

            //message.setText(text);
            message.setText(text, "ISO-8859-1", "html");

            //SEND
            Transport t = session.getTransport(mailEntity.getProtocol()); //"smtp"
            t.connect(mailEntity.getUserName(), mailEntity.getPassword()); // clave
            t.sendMessage(message, message.getAllRecipients());

            t.close();
            tokenSendMail = true;
            return tokenSendMail;
        } catch (Exception e) {
            e.printStackTrace();
            return tokenSendMail;
        }

    }
}