package com.posma.sckola.app.business.impl;


import com.posma.sckola.app.business.EvaluationPlanTemplateBF;
import com.posma.sckola.app.dto.EvaluationPlanDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.persistence.dao.EvaluationPlanDao;
import com.posma.sckola.app.persistence.dao.EvaluationPlanTemplateDao;
import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 27/04/2017.
 */
@Service
public class EvaluationPlanTemplateBFImpl implements EvaluationPlanTemplateBF {

    @Autowired
    EvaluationPlanDao evaluationPlanDao;

    @Autowired
    EvaluationPlanTemplateDao evaluationPlanTemplateDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    UserDao userDao;

    /**
     * Service query all evaluation plans from teacher in a community
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto getAll() {

        MessageDto messageDto = new MessageDto();

        List<EvaluationPlanTemplateEntity> evaluationPlanTempEntityList = evaluationPlanTemplateDao.findAll();
        List<EvaluationPlanDto> evaluationPlanDtoList = new ArrayList<EvaluationPlanDto>(evaluationPlanTempEntityList.size());

        for(EvaluationPlanTemplateEntity evaluationPlanTemplateEntity: evaluationPlanTempEntityList){
            evaluationPlanDtoList.add(entityMapper.entityToBOFull(evaluationPlanTemplateEntity));
        }

        messageDto.setResponse(evaluationPlanDtoList);

        return messageDto;
    }


    /**
     * Service create new Evaluation Plan Template from evaluation plan
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto newEvaluationPlanTemplate(Long evaluationPlanId, String evaluationPlanTemplateName) {

        MessageDto messageDto = new MessageDto();

        try {

            EvaluationPlanEntity evaluationPlanEntity = evaluationPlanDao.findOne(evaluationPlanId);

            if(evaluationPlanEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            // Realiza la copia del plan de evaluaciones
            EvaluationPlanTemplateEntity evaluationPlanTemplateEntity = copy(evaluationPlanEntity);

            // Actualiza el nombre de la plantilla del plan de evaluacion
            evaluationPlanTemplateEntity.setName(evaluationPlanTemplateName.toUpperCase());

            evaluationPlanTemplateEntity = evaluationPlanTemplateDao.create(evaluationPlanTemplateEntity);

            if (evaluationPlanTemplateEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

            messageDto.setResponse(entityMapper.entityToBOFull(evaluationPlanTemplateEntity));

        }catch(NoResultException ne){
            ne.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    private EvaluationPlanTemplateEntity copy(EvaluationPlanEntity evaluationPlanEntity){
        if(evaluationPlanEntity == null)
            return null;
        EvaluationPlanTemplateEntity evaluationPlanTemplateEntity = new EvaluationPlanTemplateEntity();
        //evaluationPlanTemplateEntity.setId(evaluationPlanEntity.getId());
        evaluationPlanTemplateEntity.setName(evaluationPlanEntity.getName());
        evaluationPlanTemplateEntity.setUser(evaluationPlanEntity.getUser());
        if(evaluationPlanEntity.getEvaluationList() != null) {
            for(EvaluationEntity evaluationEntity: evaluationPlanEntity.getEvaluationList())
                evaluationPlanTemplateEntity.addEvaluationTemplateList(this.copy(evaluationEntity, evaluationPlanTemplateEntity));
        }
        return evaluationPlanTemplateEntity;
    }

    private EvaluationTemplateEntity copy(EvaluationEntity evaluationEntity, EvaluationPlanTemplateEntity evaluationPlanTemplateEntity){
        EvaluationTemplateEntity evaluationTemplateEntity = new EvaluationTemplateEntity();
        //evaluationTemplateEntity.setId(evaluationEntity.getId());
        evaluationTemplateEntity.setObjective(evaluationEntity.getObjective());
        evaluationTemplateEntity.setDate(evaluationEntity.getDate());
        evaluationTemplateEntity.setWeight(evaluationEntity.getWeight());
        evaluationTemplateEntity.setEvaluationTool(evaluationEntity.getEvaluationTool());
        evaluationTemplateEntity.setEvaluationScale(evaluationEntity.getRatingScale());
        evaluationTemplateEntity.setStatus(evaluationEntity.getStatus());
        evaluationTemplateEntity.setEvaluationPlanTemplate(evaluationPlanTemplateEntity);

        return evaluationTemplateEntity;
    }

    /*
    private EvaluationValueTemplateEntity copy(RatingValueEntity ratingValueEntity, EvaluationScaleTemplateEntity evaluationScaleTemplateEntity){
        EvaluationValueTemplateEntity evaluationValueTemplateEntity = new EvaluationValueTemplateEntity();
        //evaluationValueTemplateEntity.setId(ratingValueEntity.getId());
        evaluationValueTemplateEntity.getQualification().setText(ratingValueEntity.getQualification().getText());
        evaluationValueTemplateEntity.getQualification().setValue(ratingValueEntity.getQualification().getValue());
        evaluationValueTemplateEntity.getQualification().setWeight(ratingValueEntity.getQualification().getWeight());
        evaluationValueTemplateEntity.setEvaluationScaleTemplate(evaluationScaleTemplateEntity);
        return evaluationValueTemplateEntity;
    }

    private EvaluationScaleTemplateEntity copy(RatingScaleEntity ratingScaleEntity){
        EvaluationScaleTemplateEntity evaluationScaleTemplateEntity = new EvaluationScaleTemplateEntity();
        //evaluationScaleTemplateEntity.setId(ratingScaleEntity.getId());
        evaluationScaleTemplateEntity.setName(ratingScaleEntity.getName());
        evaluationScaleTemplateEntity.setDescription(ratingScaleEntity.getDescription());
        for(RatingValueEntity ratingValueEntity: ratingScaleEntity.getRatingValueList()){
            evaluationScaleTemplateEntity.addEvaluationValueList(this.copy(ratingValueEntity, evaluationScaleTemplateEntity));
        }
        return evaluationScaleTemplateEntity;
    }

    private EvaluationToolTemplateEntity copy(EvaluationToolEntity evaluationToolEntity){
        EvaluationToolTemplateEntity evaluationToolTemplateEntity = new EvaluationToolTemplateEntity();
        //evaluationToolTemplateEntity.setId(evaluationToolEntity.getId());
        evaluationToolTemplateEntity.setName(evaluationToolEntity.getName());
        evaluationToolTemplateEntity.setDescription(evaluationToolEntity.getDescription());
        return evaluationToolTemplateEntity;
    }
    */
}

