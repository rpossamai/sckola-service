package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.EvaluationBF;
import com.posma.sckola.app.dto.EvaluationDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
@Service
public class EvaluationBFImpl implements EvaluationBF {

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    EvaluationDao evaluationDao;

    @Autowired
    EvaluationToolDao evaluationToolDao;

    @Autowired
    RatingScaleDao ratingScaleDao;

    @Autowired
    EvaluationPlanDao evaluationPlanDao;

    @Autowired
    EntityMapper entityMapper;

    @Override
    @Transactional
    public MessageDto evaluationGetMatterComunitySection(Long matterComunitySectionId){

        MessageDto messageDto = new MessageDto();

        try{
            MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterComunitySectionId);
            if(matterCommunitySectionEntity == null) {
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }
            if(matterCommunitySectionEntity.getEvaluationPlan() == null){
                messageDto.setMessage("No existe Un plan de evaluation");
                return messageDto;
            }
            List<EvaluationEntity> evaluationEntityList = evaluationDao.getAllEvaluationPlan(matterCommunitySectionEntity.getEvaluationPlan().getId());

            if(evaluationEntityList == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }
            List<EvaluationDto> evaluationDtoList = new ArrayList<EvaluationDto>();
            for(EvaluationEntity evaluationEntity: evaluationEntityList){
                evaluationDtoList.add(entityMapper.entityToBO(evaluationEntity));
            }

            messageDto.setResponse(evaluationDtoList);
            return messageDto;

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Search not exit");
            return messageDto;
        }
    }


    /**
     * Service create a new Evaluation
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto newEvaluation(Long evaluationPlanId, EvaluationDto evaluationDto){
        MessageDto messageDto = new MessageDto();

        EvaluationEntity evaluationEntity = new EvaluationEntity();

        try {
            EvaluationPlanEntity evaluationPlanEntity = evaluationPlanDao.findOne(evaluationPlanId);
            EvaluationToolEntity evaluationToolEntity = evaluationToolDao.findOne(evaluationDto.getEvaluationTool().getId());
            RatingScaleEntity ratingScaleEntity = ratingScaleDao.findOne(evaluationDto.getEvaluationScale().getId());
            messageDto.setIdRequest(evaluationDto.getId());

            if(evaluationPlanEntity == null || evaluationToolEntity == null || ratingScaleEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            evaluationEntity.setRatingScale(ratingScaleEntity);
            evaluationEntity.setEvaluationTool(evaluationToolEntity);
            evaluationEntity.setEvaluationPlan(evaluationPlanEntity);

            evaluationEntity = entityMapper.boToEntity(evaluationDto, evaluationEntity);

            evaluationEntity = evaluationDao.create(evaluationEntity);

            if (evaluationEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

            messageDto.setResponse(entityMapper.entityToBO(evaluationEntity));

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }


    /**
     * Servicio que permite actualizar una evaluación
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto updateEvaluation(Long evaluationId, EvaluationDto evaluationDto){
        MessageDto messageDto = new MessageDto();

        try {
            EvaluationEntity evaluationEntity = evaluationDao.findOne(evaluationId);
            EvaluationPlanEntity evaluationPlanEntity = evaluationPlanDao.findOne(evaluationDto.getEvaluationPlanId());
            EvaluationToolEntity evaluationToolEntity = evaluationToolDao.findOne(evaluationDto.getEvaluationTool().getId());
            RatingScaleEntity ratingScaleEntity = ratingScaleDao.findOne(evaluationDto.getEvaluationScale().getId());

            if(evaluationEntity == null || evaluationPlanEntity == null || evaluationToolEntity == null || ratingScaleEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            evaluationEntity.setRatingScale(ratingScaleEntity);
            evaluationEntity.setEvaluationTool(evaluationToolEntity);
            evaluationEntity.setEvaluationPlan(evaluationPlanEntity);

            evaluationEntity = entityMapper.boToEntity(evaluationDto, evaluationEntity);

            evaluationEntity = evaluationDao.update(evaluationEntity);

            if (evaluationEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

            messageDto.setResponse(entityMapper.entityToBO(evaluationEntity));

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    /**
     * Servicio que permite eliminar una evaluación de un plan de evaluaciones
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto deleteEvaluation (Long evaluationId){
        MessageDto messageDto = new MessageDto();

        messageDto.setIdRequest(evaluationId);

        try {
            evaluationDao.deleteById(evaluationId);
        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    /**
     * Servicio que permite cambia el estado de la evaluacion a culminado
     * @since 28/04/2017
     * @author Ricardo Balza
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto culminatedEvaluation (Long evaluationId){
        MessageDto messageDto = new MessageDto();

        messageDto.setIdRequest(evaluationId);

        try {
            EvaluationEntity evaluationEntity = evaluationDao.findOne(evaluationId);

            if (evaluationEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
            }

            evaluationEntity.setStatus(StatusEvaluation.CULMINATED);
            evaluationDao.update(evaluationEntity);

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }


}
