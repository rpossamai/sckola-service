package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.CommunityBF;
import com.posma.sckola.app.dto.CommunityDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Posma-ricardo on 10/04/2017.
 */
@Service
public class CommunityBFImpl implements CommunityBF{

    @Autowired
    CommunityDao communityDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    UserRoleCommunityDao userRoleCommunityDao;

    @Autowired
    UserDao userDao;

    @Autowired
    RoleDao roleDao;

    @Autowired
    SystemMessage systemMessage;

    @Override
    @Transactional
    public MessageDto createCommunity(CommunityDto communityDto){

        MessageDto messageDto = new MessageDto();

        CommunityEntity communityEntity = new CommunityEntity();

        communityEntity.setName(communityDto.getName());
        communityEntity.setDescription(communityDto.getDescription());

        try{
            communityEntity = communityDao.create(communityEntity);

            if(communityEntity != null && communityEntity.getId() != null){
                messageDto.setResponse(entityMapper.entityToBO(communityEntity));
            }else{
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter not exists");
            }

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-005");
            messageDto.setMessage(systemMessage.getMessage("SK-005"));
        }

        return messageDto;
    }

    @Override
    @Transactional
    public List<CommunityDto> getAll(){
        List<CommunityEntity> communityEntityList = communityDao.findAll();

        List<CommunityDto> communityDtoList = new ArrayList<CommunityDto>();

        for(CommunityEntity communityEntity:communityEntityList){
            communityDtoList.add(entityMapper.entityToBO(communityEntity));
        }
        return communityDtoList;
    }

    @Override
    @Transactional
    public MessageDto getById(Long communityId){

        MessageDto messageDto = new MessageDto();

        try{
            CommunityEntity communityEntity = communityDao.findOne(communityId);

            if(communityEntity == null || communityEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Community not exists");
            }

            messageDto.setResponse(entityMapper.entityToBO(communityEntity));

            return messageDto;

        }catch(Exception e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("GET-COM-USER-001");
            messageDto.setMessage(systemMessage.getErrorCode().get("GET-COM-USER-001"));
            return messageDto;
        }
    }

    @Override
    @Transactional
    public MessageDto editarcommunity(Long communityId, CommunityDto communityDto){
        MessageDto messageDto = new MessageDto();
        try{
            CommunityEntity communityEntity = communityDao.findOne(communityId);

            if(communityEntity == null || communityEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Community not exists");
            }

            communityEntity.setName(communityDto.getName());
            communityEntity.setDescription(communityDto.getDescription());

            communityEntity = communityDao.update(communityEntity);

            messageDto.setResponse(entityMapper.entityToBO(communityEntity));

            return messageDto;

        }catch(Exception e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-003");
            messageDto.setMessage(systemMessage.getErrorCode().get("SK-003"));
            return messageDto;
        }
    }


    /**
     * Servicio que permite consultar todas las comunidades donde participa un usuario del sistema segun el role (TEACHER/STUDENT)
     * @param userId
     * @param roleId
     * @return List<CommunityDto>
     */
    @Override
    @Transactional
    public MessageDto getByUserIdRoleId(Long userId, Long roleId){

        MessageDto messageDto = new MessageDto();
        List<CommunityDto> communityDtoList = new ArrayList<>();

        try {
            List<UserRoleCommunityEntity> userRoleCommunityList = userRoleCommunityDao.findAllByFieldUserIdRoleId(userId, roleId);

            for (UserRoleCommunityEntity userRoleCommunity : userRoleCommunityList) {
                communityDtoList.add(entityMapper.entityToBO(userRoleCommunity.getCommunityUser()));
            }

            messageDto.setResponse(communityDtoList);

        }catch (Exception e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("GET-COM-USER-001");
            messageDto.setMessage(systemMessage.getErrorCode().get("GET-COM-USER-001"));
        }

        return messageDto;
    }


    /**
     * Servicio que permite AsociarComunidad y Desasociar un usuario del sistema con algun role (TEACHER/STUDENT) a una comunidad.
     * Automaticamente creara una nueva entrada a la tabla COMMUNITY_USER. siendo esta la copia de la comunidad pasada por parametro.
     * @param communityId
     * @param userId
     * @param roleId
     * @return MessageDto
     */
    /*
    @Override
    @Transactional
    public MessageDto setCommunityIdUserIdRoleId(Long communityId, Long userId, Long roleId, RequestAssociateDto requestAssociateDto){
        MessageDto messageDto = new MessageDto();
        List<CommunityDto> communityDtoList = new ArrayList<>();

        UserRoleCommunityEntity userRoleCommunityEntity;

        try {
            userRoleCommunityEntity = userRoleCommunityDao.findAllByFieldCommunityIdUserIdRoleId(communityId, userId, roleId);
            if(userRoleCommunityEntity != null && userRoleCommunityEntity.getId() == null){
                userRoleCommunityEntity = null;
            }
        }catch (NoResultException e){
            userRoleCommunityEntity = null;
        }

        // Asociar una comunidad a un usuario
        if(requestAssociateDto.getAssociate()){

            if(userRoleCommunityEntity != null){
                // La asociacion ya existe:
                messageDto.setSuccess(true);
                messageDto.setResponse(userRoleCommunityEntity.getId());
                return messageDto;
            }

            CommunityEntity communityEntity;
            UserEntity userEntity;
            RoleEntity roleEntity;

            try {
                communityEntity = communityDao.findOne(communityId.longValue());
                userEntity = userDao.findOne(userId.longValue());
                roleEntity = roleDao.findOne(roleId.longValue());

                if(communityEntity == null || userEntity == null || roleEntity == null){
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("POS-COM-USER-002");
                    messageDto.setMessage(systemMessage.getMessage("POS-COM-USER-002"));
                    return messageDto;
                }

            }catch (Exception e){
                e.printStackTrace();
                messageDto.setSuccess(false);
                messageDto.setErrorCode("POS-COM-USER-002");
                messageDto.setMessage(systemMessage.getMessage("SET-COM-USER-002"));

                return messageDto;
            }

            try{

                CommunityUserEntity communityUserEntity = communityUserDao.create(new CommunityUserEntity(communityEntity));

                UserRoleCommunityEntity userRoleCommunityEntityNew = new UserRoleCommunityEntity(userEntity, roleEntity, communityEntity, communityUserEntity);

                userRoleCommunityEntity = userRoleCommunityDao.create(userRoleCommunityEntityNew);

                if(userRoleCommunityEntity != null && userRoleCommunityEntity.getId() != null) {
                    messageDto.setSuccess(true);
                    messageDto.setResponse(userRoleCommunityEntity.getId());
                }else{
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("POS-COM-USER-001");
                    messageDto.setMessage(systemMessage.getMessage("SET-COM-USER-001"));
                }
            }catch (Exception e){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("POS-COM-USER-001");
                messageDto.setMessage(systemMessage.getMessage("POS-COM-USER-001"));
            }

        }else{
            // cuando se va desasociar
            if(userRoleCommunityEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("POS-COM-USER-004");
                messageDto.setMessage(systemMessage.getMessage("POS-COM-USER-004"));
                return messageDto;
            }

            try{
                userRoleCommunityDao.delete(userRoleCommunityEntity);
                messageDto.setSuccess(true);

            }catch (Exception e){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("POS-COM-USER-005");
                messageDto.setMessage(systemMessage.getMessage("POS-COM-USER-005"));
            }
        }

        return messageDto;
    }
*/


}
