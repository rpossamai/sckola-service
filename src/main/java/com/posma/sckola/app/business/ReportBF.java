package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;

/**
 * Created by Posma-ricardo on 01/11/2017.
 */
public interface ReportBF {

    public MessageDto reportMatterSectionAverage(Long matterCommunitySectionId);
    public  MessageDto reportNoAttendance (Long matterCommunitySectionId);

    public MessageDto studentsMatterSection(Long sectionId, Long matterId);

    public MessageDto sectionsByMatterCommunity(Long matterId, Long communityId, Long teacherId);

    public MessageDto matterByCommunityTeacher(Long communityId, Long teacherId );

    MessageDto reportPlanEvaluation (Long communityId, Long matterId, Long sectionId, Long userId);

    MessageDto getMatterByCommunityUser (Long communityId, Long userId);

    MessageDto getSectionByMatterCommunity(Long sectionId, Long communityId);
}
