package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.LoginBF;
import com.posma.sckola.app.dto.LoginDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by Francis on 27/03/2017.
 */
@Service
public class LoginBFImpl implements LoginBF {

    @Autowired
    UserDao userDao;

    @Autowired
    EntityMapper entityMapper;



    /**
     * Service that create a login
     * @since 27/03/2017
     * @param loginDto
     * @return login identifier
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto login(LoginDto loginDto){

        MessageDto messageDto = new MessageDto();

        UserEntity userEntity = userDao.findByMail(loginDto.getUsername().toLowerCase());
        if(userEntity != null ) {
            if (userEntity.getStatus().equals(StatusUser.ACTIVE)) {
                if (userEntity != null && userEntity.getId() != null && userEntity.getPassword().toUpperCase().compareTo(loginDto.getPassword().toUpperCase()) == 0) {
                    messageDto.setResponse(entityMapper.entityToBO(userEntity));

                    return messageDto;

                }else{
                    messageDto.setMessage("authentication fail");
                }
            }else{
                messageDto.setMessage("user status "+ userEntity.getStatus());
            }
        }else{
            messageDto.setMessage("User or password invalid");
        }

        messageDto.setSuccess(false);
        return messageDto;
    }


}

