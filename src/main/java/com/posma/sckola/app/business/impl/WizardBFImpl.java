package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.WizardBF;
import com.posma.sckola.app.dto.*;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by Francis on 29/05/2017.
 */
@Service
public class WizardBFImpl implements WizardBF {

    @Autowired
    WizardDao wizardDao;

    @Autowired
    UserDao userDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    EvaluationPlanDao evaluationPlanDao;

    @Autowired
    CommunityDao communityDao;

    @Autowired
    EntityMapper entityMapper;

    final static String SELECT_COMMUNITY = "Selecciona tú comunidad";
    final static String IMPUT_DATA = "Ingresa tus Datos";
    final static String SELECT_MATTER = "Selecciona Materia";
    final static String LOAD_EVALUATIONS = "Cargar Evaluaciones";
    final static String LOAD_STUDENT = "Agregar Estudiantes";


    /**
     * Consulta la informacion referente al proceso del wizard para el usuario userId
     * @since 29/05/2017
     * @return MessageDto (wizardDto)
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto get(Long userId){

        MessageDto messageDto = new MessageDto();
        messageDto.setIdRequest(userId);

        WizardEntity wizardEntity;

        List<WizardEntity> wizardEntityList = wizardDao.findAllByFieldId("USER_ID", userId);

        if(wizardEntityList.size() >= 1) {
            wizardEntity = wizardEntityList.get(0);
            messageDto.setResponse(entityMapper.entityToBO(wizardEntity));
            return messageDto;
        }else{
            messageDto.setMessage("La entrada del usuario ya no existe en el wizard");
            messageDto.setResponse(userId);
            return messageDto;
        }
    }

    /**
     * Crea una nueva entrada en el wizard para el usuario userEntity
     * @since 29/05/2017
     * @param userEntity
     * @return login identifier
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto save(UserEntity userEntity){

        MessageDto messageDto = new MessageDto();

        WizardEntity wizardEntity = new WizardEntity(userEntity);
        wizardEntity = wizardDao.create(wizardEntity);

        if(wizardEntity != null){
            messageDto.setResponse(entityMapper.entityToBO(wizardEntity));
        }else{
            messageDto.setSuccess(false);
        }
        return messageDto;
    }



    /**
     * Service that update step name from wizard
     * @since 29/05/2017
     * @param wizardDto
     * @return MessageDto (wizardDto)
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto update(WizardDto wizardDto){

        MessageDto messageDto = new MessageDto();
        WizardEntity wizardEntity;

        List<WizardEntity> wizardEntityList = wizardDao.findAllByFieldId("USER_ID", wizardDto.getUserId().longValue());

        if(wizardEntityList.size() >= 1) {
            wizardEntity = wizardEntityList.get(0);

            if(wizardDto.getCommunity() != null && wizardDto.getCommunity().getId() != null){
                CommunityEntity communityEntity = communityDao.findOne(wizardDto.getCommunity().getId());
                wizardEntity.setCommunity(communityEntity);
            }

            if(wizardDto.getMatterCommunitySection() != null && wizardDto.getMatterCommunitySection().getId() != null){
                MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(wizardDto.getMatterCommunitySection().getId());
                wizardEntity.setMatterCommunitySection(matterCommunitySectionEntity);
            }

            if(wizardDto.getEvaluationPlan() != null && wizardDto.getEvaluationPlan().getId() != null){
                EvaluationPlanEntity evaluationPlanEntity = evaluationPlanDao.findOne(wizardDto.getEvaluationPlan().getId());
                wizardEntity.setEvaluationPlan(evaluationPlanEntity);
            }

            wizardEntity = wizardDao.update(wizardEntity);
            messageDto.setResponse(entityMapper.entityToBO(wizardEntity));
            return messageDto;
        }else{
            messageDto.setSuccess(false);
            messageDto.setMessage("Invalido parametros de entrada");
            messageDto.setResponse(wizardDto);
            return messageDto;
        }
    }


    /**
     * Cierra el ciclo del wizard
     * @since 29/05/2017
     * @param wizardDto
     * @return MessageDto (UserDto)
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto finish(WizardDto wizardDto){

        MessageDto messageDto = new MessageDto();
        WizardEntity wizardEntity;

        List<WizardEntity> wizardEntityList = wizardDao.findAllByFieldId("USER_ID", wizardDto.getUserId().longValue());

        if(wizardEntityList.size() >= 1) {
            wizardEntity = wizardEntityList.get(0);

            UserEntity userEntity = wizardEntity.getUser();
            userEntity.setStatus(StatusUser.ACTIVE);
            userEntity = userDao.update(userEntity);
            wizardDao.delete(wizardEntity);

            messageDto.setResponse(entityMapper.entityToBO(userEntity));
            return messageDto;
        }else{
            messageDto.setMessage("La entrada del usuario ya no existe en el wizard");
            messageDto.setResponse(wizardDto);
            return messageDto;
        }
    }


    @Override
    @Transactional
    public MessageDto updateCommunity(Long idCommunity, Long idUser) {

        WizardEntity wizardEntity;
        MessageDto messageDto = new MessageDto();

        List<WizardEntity> wizardEntityList = wizardDao.findAllByFieldId("USER_ID", idUser);

        if (wizardEntityList.size() >= 1) {
            wizardEntity = wizardEntityList.get(0);

            if (idCommunity != null) {
                CommunityEntity communityEntity = communityDao.findOne(idCommunity);
                wizardEntity.setCommunity(communityEntity);
            }

            wizardEntity = wizardDao.update(wizardEntity);
            messageDto.setResponse(entityMapper.entityToBO(wizardEntity));
            return messageDto;
        } else {
            messageDto.setSuccess(false);
            messageDto.setMessage("Invalido parametros de entrada");
            return messageDto;
        }
    }


    @Override
    @Transactional
    public MessageDto updateProfileUser(Long idUser) {

        WizardEntity wizardEntity;
        MessageDto messageDto = new MessageDto();

        List<WizardEntity> wizardEntityList = wizardDao.findAllByFieldId("USER_ID", idUser);

        if (wizardEntityList.size() >= 1) {
            wizardEntity = wizardEntityList.get(0);

            wizardEntity.setUserProfile(idUser);
            wizardEntity = wizardDao.update(wizardEntity);

            messageDto.setResponse(entityMapper.entityToBO(wizardEntity));
            return messageDto;
        } else {
            messageDto.setSuccess(false);
            messageDto.setMessage("Invalido parametros de entrada");
            return messageDto;
        }
    }


}

