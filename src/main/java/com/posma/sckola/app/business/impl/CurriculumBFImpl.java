package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.CurriculumBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.CurriculumDto;
import com.posma.sckola.app.persistence.dao.CommunityDao;
import com.posma.sckola.app.persistence.dao.CurriculumDao;
import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.entity.CommunityEntity;
import com.posma.sckola.app.persistence.entity.CurriculumEntity;
import com.posma.sckola.app.persistence.entity.UserEntity;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Service
public class CurriculumBFImpl implements CurriculumBF {

    @Autowired
    CurriculumDao curriculumDao;

    @Autowired
    UserDao userDao;

    @Autowired
    CommunityDao communityDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    Validation validation;

    /**
     * Service to create curriculum
     * @since 20/04/2017
     * @param curriculumDto
     * @param userId
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto curriculumCreate(CurriculumDto curriculumDto, Long userId) {

        MessageDto messageDto = new MessageDto();

        try {
            UserEntity userEntity = userDao.findOne(userId.longValue());


            CurriculumEntity curriculumEntity = new CurriculumEntity();

            if(curriculumDto.getCommunity() != null && curriculumDto.getCommunity().getId() != null) {
                CommunityEntity communityEntity = communityDao.findOne(curriculumDto.getCommunity().getId().longValue());

                if(communityEntity != null){
                    curriculumEntity.setCommunity(communityEntity);
                }
            }

            if (userEntity != null){

                curriculumEntity.setTitle(curriculumDto.getTitle());
                curriculumEntity.setDate(validation.stringToDate(curriculumDto.getDate()));
                curriculumEntity.setUser(userEntity);
                curriculumEntity.setInstitute(curriculumDto.getInstitute());

                curriculumEntity = curriculumDao.create(curriculumEntity);

                if (curriculumEntity != null && curriculumEntity.getId() != null) {
                    messageDto.setMessage("Curriculum created");
                    messageDto.setResponse(entityMapper.entityToBO(curriculumEntity));
                }else{
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("SK-001");
                    messageDto.setMessage(systemMessage.getMessage("SK-001"));
                }
            }else{
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
            }

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
        }catch (PersistenceException pe){
            pe.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-001");
            messageDto.setMessage(systemMessage.getMessage("SK-001"));
        }

        return messageDto;
    }


 
    /**
     * Service to update data from curriculum
     * @since 19/04/2017
     * @param curriculumId curriculum identifier
     * @param curriculumDto curriculum dto with all params to update
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto curriculumUpdate(Long curriculumId, CurriculumDto curriculumDto){
        MessageDto messageDto = new MessageDto();

        CurriculumEntity curriculumEntity;
        UserEntity userEntity;
        CommunityEntity communityEntity;

        try{
            // busca el curriculo que se quiere actualizar
            curriculumEntity = curriculumDao.findOne(curriculumId);

            if (curriculumEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Curriculum not exists");
                return messageDto;
            }
        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Curriculum not exists");

            return messageDto;
        }

        try{
            userEntity = userDao.findOne(curriculumDto.getUserId().longValue());

            if(userEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - User not exists");
                return messageDto;
            }

            if(curriculumDto.getCommunity() != null && curriculumDto.getCommunity().getId() != null) {
                communityEntity = communityDao.findOne(curriculumDto.getCommunity().getId().longValue());
                if(communityEntity == null) {
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("SK-002");
                    messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Community not exists");
                    return messageDto;
                }else{
                    curriculumEntity.setCommunity(communityEntity);
                }
            }

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - User or Community not exists");
            return messageDto;
        }

        try{
            curriculumEntity.setTitle(curriculumDto.getTitle());
            curriculumEntity.setDate(validation.stringToDate(curriculumDto.getDate()));
            curriculumEntity.setUser(userEntity);
            curriculumEntity.setInstitute(curriculumDto.getInstitute());

            curriculumEntity = curriculumDao.update(curriculumEntity);

            if (curriculumEntity != null && curriculumEntity.getId() != null) {
                messageDto.setMessage("Curriculum Update");
                messageDto.setResponse(entityMapper.entityToBO(curriculumEntity));
            }else{
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-003");
                messageDto.setMessage(systemMessage.getMessage("SK-003"));
            }

        }catch (PersistenceException pe){
            pe.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-001");
            messageDto.setMessage(systemMessage.getMessage("SK-001"));
        }

        return messageDto;
    }

}

