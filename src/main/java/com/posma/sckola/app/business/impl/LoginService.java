package com.posma.sckola.app.business.impl;


import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.entity.UserEntity;
import com.posma.sckola.app.security.JwtUserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Francis Ries on 22/05/2017.
 */
@Service
@Transactional(readOnly = true)
public class LoginService implements UserDetailsService {

    @Autowired
    UserDao userDao ;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userDao.findByMail(username.toUpperCase());
        return JwtUserFactory.create(user);
    }
}
