package com.posma.sckola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;

/**
 * Created by PosmaGroup on 02/11/2017.
 */
@SpringBootApplication(scanBasePackages = {"com.posma.sckola"},
        exclude = {SecurityAutoConfiguration.class })
public class SkolaApplication {
    /**
     * Main method.
     */
    public static void main(String[] args) {
        SpringApplication.run(SkolaApplication.class, args);
    }
}
