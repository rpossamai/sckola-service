package com.posma.sckola.config;

/**
 * Created by francis on 27/03/2017.
 */
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import com.posma.sckola.app.util.ServiceProperties;
import com.posma.sckola.app.util.SystemMessage;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.posma.sckola.app.init.DataInitializer;



@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.posma.sckola.config" })
@PropertySource(value = { "classpath:application.properties" })
public class JPAConfiguration {

    @Autowired
    private Environment environment;

    @Autowired
    private DataSource dataSource;

    // Carga los codigos y mensajes de error del systema en memoria
    @Bean
    public SystemMessage systemMessage() {
        return new SystemMessage();
    }

    @Bean(initMethod = "init")
    public DataInitializer dataInitializer() {
        return new DataInitializer();
    }

    @Bean
    @DependsOn("dataSource")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        //Set up de conexion JNDI o  JDBC
        em.setDataSource(dataSource);
        em.setPackagesToScan(new String[] {"com.posma.sckola.app.persistence.entity"});
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());
        return em;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
        return new PersistenceExceptionTranslationPostProcessor();
    }


    @Bean
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.auto"));
        properties.setProperty("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
        properties.setProperty("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        properties.setProperty("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
        return properties;
    }

    @Bean
    public ServiceProperties serviceProperties() {
        return new ServiceProperties(environment.getRequiredProperty("urlServerBusiness"),
                environment.getRequiredProperty("urlServerFront"),
                environment.getRequiredProperty("urlLogo"));
    }

    @Bean(initMethod = "migrate")
    @DependsOn("dataSource")
    public Flyway flyway() {
        Flyway flyway = new Flyway();
        flyway.setBaselineOnMigrate(true);
        flyway.setDataSource(dataSource);
        flyway.configure(additionalProperties());
        flyway.setLocations("/scripts/");
        return flyway;
    }

/*
    @Bean(initMethod = "init")
    public AuditModule auditModule() {
        return new AuditModule();
    }*/

}
